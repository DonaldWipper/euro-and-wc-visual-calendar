![Python](https://img.shields.io/badge/python-v3.8+-blue.svg)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)


# Проект "Визуализация календарей чемпионатов мира и европы по футболу" 
<a href="http://euro-stat.com/" target="_blank">ССЫЛКА НА ПРОЕКТ</a>

Проект представляет собой наглядную демонстрацию всей основной информации о Чемпионатах мира и Европы по футболу, начиная с самого первого официального мундиаля, который состоялся в 1930 году в Уругвае. Здесь можно узнать о времени и местах проведения, странах-участницах и результатах матчей. Помимо этого, в проекте содержатся подробности о каждой отдельно взятой игре, в частности время, в которое были забиты мячи и составы команд. Данные по текущему чемпионату отображаются в режиме реального времени.

![Промо ролик](media/promo.mp4)
<a href="https://www.youtube.com/watch?v=Vxa5n4IoPmA" target="_blank">ОПИСАНИЕ YOUTUBE</a>

## Как смотреть визуализацию?
Интерфейс программы представляет собой эллипсоидную диаграмму с наборов кусочков (слайсов).
Каждый кусочек выделен в отдельную группу (команды, расписания матчей, стадионы и города, группы и стадии турнира)
Клик на каждый кусочек диаграммы выделяет связанную структуру данных с этим событием.
А именно. **Клик** на:
* __Команду__. Выделяет:
дни, по которым будут сыграны матчи,
стадионы игр, группа.
__В центре__ показывается расписание всех матчей.
* __День игры__. Выделяет все команды, играющие в этот день, стадионы игр,
группы, играющие в этот день
В центре показывается расписание всех матчей в этот день.
* __Стадион__ 
* __Стадия__

Помимо локальных кусочков. Есть события при нажатии внешних арок.
1. Нажатие на «Национальные команды» показывает в центре карту мира с выделенными странами-участниками турнира;
2. Нажатие на «Расписание» календарь всех игровых дней;
3. Нажатие на «Города и стадионы» показывает отмеченные города на карте страны, принимающей турнир; 
4. Нажатие на «Группы и стадии» показывает полную сетку турнира.


## Архитектура проекта
Backend написан на питоне с использованием фреймворка Flask. Frontend сделан на js
c использованием библиотек  <a href="https://d3js.org/" target="_blank">d3.js</a>
для отрисовки визуализации эллипса, <a href="http://datamaps.github.io/" target="_blank">datamaps.js</a>
для отображения карт и <a href="https://gojs.net/latest/index.html" target="_blank">go.js</a> для диаграммы плейофф


```mermaid
flowchart TD

    Database[(CRUD <br> DATABASE  <br> MYSQL)] --> B[[FLASK WEBAPP  <br> Python, Flask]]
    
    B --> |Jinja| C[[Frontend  <br> html,css,js]]
    C <--> D([User])
  
```

## Источники данных
Данные парсились с сайтов:
- https://www.uefa.com/
- https://www.fifa.com/
- https://openfootball.github.io/

## Схема базы данных
8 таблиц, связанных между собою внешними ключами, cхема данных звезда с таблицей фактов - Games:
ddl таблиц хранится в `stat_app/ddl`
1. Tournament.   Рассматриваемый турнир
2. Teams. Команды ЧМ, url иконки и код страны (нужно в отрисовки карты)
3. Games (таблица фактов). Сыгранные или в расписании матчи, забитые/пропущенные голы, команда номинальный хозяин/команда гость
4. Stages. Все раунды. Групповой этап/четвертьфинал/полуфинал/финал.
5. Places.  Место, где сыгран матч, cтадион, вместимость стадиона, название стадиона и положение на карте мира.
6. Players. Футболисты команд, их позиция на поле, возраст, рост и клубная команда.


```mermaid
erDiagram
    GAMES ||--o{ PLACES : where_was_played
    GAMES ||--o{ TOURNAMENTS : which_tournament
    GAMES ||--o{ STAGES : which_stage_of_tournament
    GAMES ||--o{ TEAMS : which_teams
    PLAYERS ||--o{ TEAMS : which_teams
    GOALS ||--o{ TEAMS : which_teams
    PLACES ||--o{ TOURNAMENTS : which_tournament
    STAGES ||--o{ TOURNAMENTS : which_tournament
    GOALS ||--o{ TOURNAMENTS : which_tournament
    PLAYERS ||--o{ TOURNAMENTS : which_tournament
    
    GAMES {
        int id PK
        bigint competition_id FK 
        int goals_home_team
        bigint place_id FK 
        int away_team_id FK 
        int goals_away_team
        text status
        bigint home_team_id FK 
        text date
        bigint id_stage FK 
        int extra_time_home_goals
        int extra_time_away_goals
        int penalty_shootout_home_goals
        int penalty_shootout_away_goals
        bigint match_id_uefa FK 
    }
    
    PLACES {
        bigint  id  PK
        text  stadium
        text  city
        text  short_name
        int  capacity
        bigint  competition_id FK 
        double  lat
        double  lng
    }

    PLAYERS {
        bigint  id PK
        int  number
        text  href
        text  image
        text  name
        text  position
        bigint  team_id FK 
        bigint  tournament_id FK 
        text  birthdate
    }
    
    STAGES {
        int  id PK
        bigint  competition_id FK 
        text  title
        bigint  pos
    }
    
    TEAMS {
        bigint  id PK
        text   short_name
        text   short_name2
        text   name
    }
    
    TOURNAMENTS {
        bigint   id PK
        bigint   number_of_games
        bigint   number_of_match_days
        bigint   number_of_teams
        text   last_updated
        text   caption
        int    year
        text   league
        text   url
    }
    
    GOALS {
            text    guid PK
            bigint  game_id  FK 
            text    minute
            text    player_name
            text    match_link
            bigint  team_id  FK 
            bigint  player_id  FK 
            bigint  competition_id  FK 
    }
```

## Планы по доработки проекта
- [ ] Рефакторинг кода. Код проекта был написан на "коленке". Планируется переписать как frontend часть js, так и бекенд python
- [ ] Доработка интерфейса и фикс багов в отображении
- [ ] Перевод интерфейса на другие языки
- [ ] Настройка CI-CD с тестированием
- [ ] Добавить стриминговые инструменты (типа RabbitMQ или Kafka) или для получения данных в БД в режиме real-time
- [ ] Поправить хранилище данных. Типы данных, названия сущность, внешние ключи, constraints.
- [ ] Сделать открытое API при помощи Fast API для данных по ЧМ и ЧЕ
- [ ] Провести нагрузочное тестирование
- [ ] Написать блог на sports.ru, отдать сайт в открытый доступ
- [ ] Сделать возможность пожертований на сайте
- [DONE] Перейти на https c  http
- [ ] Поменять названия
- [ ] Дата поменять на нормальный формат
- [ ] Убрать день недели
- [ ] ANS -> AND
- [ ] Выделить страну при выделении
- [ ] При выделении 
