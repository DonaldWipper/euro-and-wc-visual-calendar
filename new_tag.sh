#!/bin/bash

CURTAG=`git describe --tags  --abbrev=0 --match "$1*-v*" $(git rev-list --tags --max-count=1)`;
# echo $CURTAG


function check_tag_exists(){
  my_array=( `git tag | grep $1-v` )
  for element in "${my_array[@]}"
  do
     if [ "${element}" == "${result}" ]; then
         exists=1
         return
     fi
  done
  exists=0
}

function try_get_new_version() {
    delimiter=.
    array=($(echo "$Version" | tr $delimiter '\n'))
    array[$2]=$((array[$2]+1))
    NewVersion=$(IFS=$delimiter ; echo "${array[*]}")
    result=$1-v$NewVersion
    # echo $result
}

function get_new_version() {
  exists=1
  while [ "${exists}" == 1 ]; do
    try_get_new_version $1 $2
    check_tag_exists $1 $result
    Version=$NewVersion
  done

}

MainPart=${CURTAG%-v*}
Version=${CURTAG##*-v}

#echo $MainPart
#echo $Version

get_new_version $1 $2

ETL_VERSION=$NewVersion

echo $NewVersion
echo "git tag $result && git push origin $result"
git tag $result && git push origin $result
