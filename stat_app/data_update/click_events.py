from utils import get_normal_date


def get_all_corell_by_team_id(team_id, games):
    team_id = str(team_id)
    places = [
        g["place_id"]
        for g in games
        if g["home_team_id"] == team_id or g["away_team_id"] == team_id
    ]

    dates = [
        get_normal_date(g["date"])
        for g in games
        if g["away_team_id"] == team_id or g["home_team_id"] == team_id
    ]

    _stages = [
        "s" + str(d["id_stage"])
        for d in games
        if d["away_team_id"] == team_id or d["home_team_id"] == team_id
    ]

    dic_slice_2_games[dic_name2sliceId[team_id]] += [
        g["id"] for g in games if g["away_team_id"] == team_id or g["home_team_id"] == team_id
    ]
    names = dates + _stages + places
    sliceIds = []
    for name in names:
        if name in dic_name2sliceId:
            sliceIds.append(dic_name2sliceId[name])
    return sliceIds
