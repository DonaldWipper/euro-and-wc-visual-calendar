"""
Data-class for init dashboard
"""
from dataclasses import dataclass, field
from db_connection.sql import DBConnection
from operator import itemgetter
from data_update.utils import sort_by_len_from_mid

GOALS_COLUMN = "goals%steam"
GOALS_EXTRA_COLUMN = "extra_time%sgoals"
PENALTY_COLUMN = "penalty_shootout%sgoals"

HOME_STR = 'Home'
AWAY_STR = 'Away'


def get_result_string(g):
    if g[GOALS_COLUMN % HOME_STR] is None or g[GOALS_COLUMN % AWAY_STR] is None:
        return None

    if g[GOALS_EXTRA_COLUMN % HOME_STR] is not None:
        if g[PENALTY_COLUMN % HOME_STR] is not None:
            return "%s(%s):%s(%s)" % (g[GOALS_EXTRA_COLUMN % HOME_STR],
                                      g[PENALTY_COLUMN % HOME_STR],
                                      g[GOALS_EXTRA_COLUMN % AWAY_STR],
                                      g[PENALTY_COLUMN % AWAY_STR])

        else:
            return "%s:%s" % (g[GOALS_EXTRA_COLUMN % HOME_STR], g[GOALS_EXTRA_COLUMN % AWAY_STR])
    else:
        return "%s:%s" % (g[GOALS_COLUMN % HOME_STR], g[GOALS_COLUMN % AWAY_STR])


@dataclass
class Dashboard:
    teams: list[dict]
    rounds: list[dict]
    stages: list[dict]
    games: list[dict]
    tournaments: list[dict]
    tournament_pos: int

    # spaces: list[dict]
    # out_groups: list[dict]
    # click_events: list[dict]
    # slice_name: list[dict]
    # games_playoff: list[dict]

    def __init__(self, competition_id: int = None):
        self.tournaments = DBConnection.get_dict_from_query_res("tournaments")
        self.update_tournament()

        if not competition_id:
            self.competition_id = self.tournaments[0]["id"]
        else:
            self.competition_id = competition_id

        self.places = DBConnection.get_dict_from_query_res(
            "places", {"competition_id": self.competition_id}
        )
        self.rounds = DBConnection.get_dict_from_query_res(
            "rounds", {"competition_id": self.competition_id}
        )
        self.stages = DBConnection.get_dict_from_query_res(
            "stages", {"competition_id": self.competition_id}
        )
        self.games = DBConnection.get_dict_from_query_res(
            "games", {"competition_id": self.competition_id}
        )
        self.update_games()

        self.teams = DBConnection.get_dict_from_query_res("teams")

        self.update_teams()

    def update_tournament(self):
        self.tournaments = sorted(
            self.tournaments, key=itemgetter("year"), reverse=True
        )

        i = 0
        for t in self.tournaments:
            t["caption"] = str(t["year"]) + " " + t["caption"].replace(str(t["year"]), "")
            t["position"] = i
            i += 1

    @property
    def tournament_pos(self):
        return [tour["id"] for tour in self.tournaments if tour["id"] == self.competition_id][0]

    def update_teams(self):

        teams_ids = set(
            [g["home_team_id"] for g in self.games]
            + [g["away_team_id"] for g in self.games]
        )

        teams = [
            team
            for team in DBConnection.get_dict_from_query_res("teams")
            if str(team["id"]) in teams_ids
        ]

        teams = sorted(teams, key=lambda x: -len(x["name"]))
        self.teams = sort_by_len_from_mid(teams)

    def update_games(self):
        for game in self.games:
            try:
                p = [p for p in self.places if p["id"] == game["place_id"]][0]
                game["stadium"] = p["stadium"].split("|")[0]
                game["city"] = p["city"]

            except KeyError:
                game["stadium"] = None
                game["city"] = None

            game["teamAway"] = None

            try:
                game["teamHome"] = [t for t in self.teams if str(t["id"]) == game["home_team_id"]][0][
                    "name"
                ]
            except:
                game["teamHome"] = None

            try:
                game["teamAway"] = [t for t in self.teams if str(t["id"]) == game["away_team_id"]][0][
                    "name"
                ]
            except:
                game["teamAway"] = None

            if game["goals_away_team"] is None:
                game["result"] = game["status"][0:5]
            else:
                game["result"] = get_result_string(game)

            game["date"] = game["date"][5:7] + "." + game["date"][8:10]
            game["time"] = game["date"][11:16]

            game["goals_home_team_extra"] = game["result"].split(':')[0] if ":" in game["result"] else ''
            game["goals_away_team_extra"] = game["result"].split(':')[1] if ":" in game["result"] else ''

            game["id_stage"] = game["id_stage"]
