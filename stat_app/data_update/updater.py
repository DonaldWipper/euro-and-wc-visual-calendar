import requests

token = "efda008899e346f693efa9c75f9577ee"
myheaders = {"X-Auth-Token": token, "X-Response-Control": "minified"}
main_url = "http://api.football-data.org"

global games, games_update, games_update2

def get_update_data_by_league_id(Idleague):

    url = main_url + "/v1/competitions/" + str(Idleague) + "/fixtures"
    resp = requests.get(url, headers=myheaders)
    resp = resp.json()["fixtures"]
    res_update = [
        {
            "id": str(r["home_team_id"])
                  + str(r["away_team_id"])
                  + get_normal_date(r["date"]),
            "id2": str(r["date"]),
            "away_team_id": r["away_team_id"],
            "home_team_id": r["home_team_id"],
            "status": r["status"],
            "date": r["date"],
            "goals_home_team": r["result"]["goals_home_team"],
            "goals_away_team": r["result"]["goals_away_team"],
            "penalty_shootout_home_goals": r["result"]
            .get("penaltyShootout", {})
            .get("goals_home_team", None),
            "penalty_shootout_away_goals": r["result"]
            .get("penaltyShootout", {})
            .get("goals_away_team", None),
            "extra_time_home_goals": r["result"]
            .get("extraTime", {})
            .get("goals_home_team", None),
            "extra_time_away_goals": r["result"]
            .get("extraTime", {})
            .get("goals_away_team", None),
        }
        for r in resp
        if (r["status"] != "TIMED" and r["status"] != "SCHEDULED")
           or int(r["home_team_id"]) != 757
    ]

    for r in res_update:
        if r["id"] in games_update:
            db.updateTableFromConditions(
                "games",
                {
                    "competition_id": competition_id,
                    "home_team_id": r["home_team_id"],
                    "away_team_id": r["away_team_id"],
                    "date": r["date"],
                },
                {
                    "status": r["status"],
                    "goals_home_team": r["goals_home_team"],
                    "goals_away_team": r["goals_away_team"],
                },
            )
            games = db.get_dict_from_query_res(
                "games", {"competition_id": competition_id}
            )
        if r["id2"] in games_update2:
            if r["goals_home_team"] != None:
                db.updateTableFromConditions(
                    "games",
                    {"competition_id": competition_id, "date": r["date"]},
                    {
                        "status": r["status"],
                        "goals_home_team": r["goals_home_team"],
                        "goals_away_team": r["goals_away_team"],
                        "home_team_id": r["home_team_id"],
                        "away_team_id": r["away_team_id"],
                        "penalty_shootout_home_goals": r["penalty_shootout_home_goals"],
                        "penalty_shootout_away_goals": r["penalty_shootout_away_goals"],
                        "extra_time_home_goals": r["extra_time_home_goals"],
                        "extra_time_away_goals": r["extra_time_away_goals"],
                    },
                )
            else:
                db.updateTableFromConditions(
                    "games",
                    {"competition_id": competition_id, "date": r["date"]},
                    {
                        "status": r["status"],
                        "home_team_id": r["home_team_id"],
                        "away_team_id": r["away_team_id"],
                        "penalty_shootout_home_goals": r["penalty_shootout_home_goals"],
                        "penalty_shootout_away_goals": r["penalty_shootout_away_goals"],
                        "extra_time_home_goals": r["extra_time_home_goals"],
                        "extra_time_away_goals": r["extra_time_away_goals"],
                    },
                )
            games = db.get_dict_from_query_res(
                "games", {"competition_id": competition_id}
            )

    return res_update
