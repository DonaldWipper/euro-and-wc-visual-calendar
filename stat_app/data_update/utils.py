from datetime import datetime


def get_normal_date(date_str):
    return str(datetime.strptime(date_str.strip()[0:10], "%Y-%m-%d"))[0:10]


def sort_by_len_from_mid(array):
    """
    :param array:
    :return:
    """
    mid = int(len(array) / 2)

    teams_copy = array.copy()
    teams_copy[mid] = array[0]
    shift = 1
    i = 1

    while mid - shift >= 0 or mid + shift <= len(array) - 1:
        if mid - shift >= 0:
            teams_copy[mid - shift] = array[i]

        if mid + shift <= len(array) - 1:
            teams_copy[mid + shift] = array[i + 1]

        i += 2
        shift += 1
    return teams_copy
