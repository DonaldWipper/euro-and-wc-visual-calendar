ALTER TABLE euro_stat.games_new
    ADD FOREIGN KEY (competition_id) REFERENCES tournaments_new(id);

ALTER TABLE euro_stat.games_new
    ADD FOREIGN KEY (id_stage) REFERENCES stages_new(id);

ALTER TABLE euro_stat.games_new
    ADD FOREIGN KEY (place_id) REFERENCES places_new(id);

ALTER TABLE euro_stat.games_new
    ADD FOREIGN KEY (away_team_id) REFERENCES teams_new(id);

ALTER TABLE euro_stat.games_new
    ADD FOREIGN KEY (home_team_id) REFERENCES teams_new(id);