SELECT DISTINCT cast(replace(replace(g.minute, 'PEN', ''), 'OG', '') AS signed) AS MINUTE,
                g.minute AS goal_time,
                p.name AS player_name,
                f.datetime,
                f.stadium,
                f.city,
                f.group_name,
                f.teamHome,
                f.shortHomeTeam,
                f.shortAwayTeam,
                f.teamAway,
                p.number,
                p.image,
                p.position,
                p.birthdate
FROM goals g
    JOIN games_fifa_all f ON g.matchId = f.match_fifa_id
    JOIN players p ON p.playerId = g.playerId