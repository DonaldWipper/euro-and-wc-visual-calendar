flask==2.2.2
gunicorn==20.1.0
pymysql==1.0.2
sqlalchemy==1.4.44
requests==2.28.1
pydantic==1.10.8
Werkzeug==2.2.2