from setuptools import setup, find_packages

from pkg_resources import Requirement, parse_requirements


def load_requirements(f_name: str) -> list:
    requirements = []
    with open(f_name, 'r') as fp:
        req: Requirement
        for req in parse_requirements(fp.read()):
            extras = '[{}]'.format(','.join(req.extras)) if req.extras else ''
            requirements.append(
                f'{req.project_name}{extras}{req.specs[0][0]}{req.specs[0][1]}'
            )
    return requirements

setup(
    name='euro-and-wc-visual-calendar',
    version='2.0.0',

    packages=find_packages(),
    install_requires=load_requirements('requirements.txt'),
    extras_require={
        'dev': load_requirements('dev-requirements.txt'),
    },
    include_package_data=True,
    package_data={
        '': ['requirements.txt', 'dev-requirements.txt']
    },
    entry_points={
        'console_scripts': [
        ]
    }
)


