create table games
(
    competition_id              bigint null,
    goals_home_team             text   null,
    place_id                     bigint null,
    away_team_id                text   null,
    goals_away_team             text   null,
    status                      text   null,
    home_team_id                text   null,
    date                        text   null,
    short_date                  text   null,
    id_stage                    bigint null,
    extra_time_home_goals       int    null,
    extra_time_away_goals       int    null,
    penalty_shootout_home_goals int    null,
    penalty_shootout_away_goals int    null,
    match_id_uefa               bigint null
);

