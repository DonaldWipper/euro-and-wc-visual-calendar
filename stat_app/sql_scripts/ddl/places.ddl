create table places
(
    stadium       text   null,
    city          text   null,
    id            bigint null,
    short_name    text   null,
    capacity      double null,
    competition_id bigint null,
    lat           double null,
    lng           double null
);

