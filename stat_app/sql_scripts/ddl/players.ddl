create table players
(
    playerId     bigint null,
    number       double null,
    href         text   null,
    image        text   null,
    name         text   null,
    position     text   null,
    teamId       bigint null,
    tournamentId bigint null,
    birthdate    text   null
);

