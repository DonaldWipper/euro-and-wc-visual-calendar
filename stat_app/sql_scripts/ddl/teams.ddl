create table teams
(
    shortName        text   null,
    id               bigint null,
    name             text   null,
    squadMarketValue text   null,
    crestUrl         text   null,
    rating           text   null
);

