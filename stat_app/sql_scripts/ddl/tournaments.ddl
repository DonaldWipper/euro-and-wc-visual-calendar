create table tournaments
(
    numberOfGames     bigint null,
    currentMatchday   bigint null,
    numberOfMatchdays bigint null,
    numberOfTeams     bigint null,
    id                bigint null,
    lastUpdated       text   null,
    caption           text   null,
    year              bigint null,
    league            text   null,
    Url               text   null
);
