create table games_new
(
    id                          int unique NOT NULL AUTO_INCREMENT,
    competition_id              bigint null,
    goals_home_team             int    null,
    place_id                    bigint null,
    away_team_id                int    null,
    goals_away_team             int    null,
    status                      text   null,
    home_team_id                bigint null,
    date                        text   null,
    id_stage                    bigint null,
    extra_time_home_goals       int    null,
    extra_time_away_goals       int    null,
    penalty_shootout_home_goals int    null,
    penalty_shootout_away_goals int    null,
    match_id_uefa               bigint null
);

create table places_new
(
    id             bigint unique not null,
    stadium        text   null,
    city           text   null,
    short_name     text   null,
    capacity       int    null,
    competition_id bigint null,
    lat            double null,
    lng            double null
);

create table players_new
(
    id            bigint unique not null,
    number        int           null,
    href          text          null,
    image         text          null,
    name          text          null,
    position      text          null,
    team_id       bigint        null,
    tournament_id bigint        null,
    birthdate     text          null
);

create table stages_new
(
    id             int unique    NOT NULL AUTO_INCREMENT,
    competition_id bigint null,
    title          text          null,
    pos            bigint        null
);

create table teams_new
(
    id          bigint unique null,
    short_name  text          null,
    short_name2 text          null,
    name        text          null
);

create table tournaments_new
(
    id                   bigint unique null,
    number_of_games      bigint        not null,
    number_of_match_days bigint        not null,
    number_of_teams      bigint        not null,
    last_updated         text          not null,
    caption              text          not null,
    year                 int           not null,
    league               text          not null,
    url                  text          null
);