document.addEventListener('DOMContentLoaded', function() {
    function toggleMenuItems() {
        const menuItems = document.querySelector('.menu-items');
        menuItems.style.display = menuItems.style.display === 'none' ? 'block' : 'none';
    }

    function toggleSeasonsList(seasonsList) {
        seasonsList.style.display = seasonsList.style.display === 'none' ? 'block' : 'none';
    }

    const burger = document.querySelector('.burger');
    burger.addEventListener('click', toggleMenuItems);

    const TournamentData = {
        tournaments: [
            {
                name: 'Premier League',
                seasons: ['2021/22', '2020/21', '2019/20', '2018/19', '2017/18'],
            },
            {
                name: 'La Liga',
                seasons: ['2021/22', '2020/21', '2019/20', '2018/19', '2017/18'],
            },
            {
                name: 'Bundesliga',
                seasons: ['2021/22', '2020/21', '2019/20', '2018/19', '2017/18'],
            },
            {
                name: 'Serie A',
                seasons: ['2021/22', '2020/21', '2019/20', '2018/19', '2017/18'],
            },
            {
                name: 'Ligue 1',
                seasons: ['2021/22', '2020/21', '2019/20', '2018/19', '2017/18'],
            },
        ],
    };

    const tournaments = TournamentData.tournaments;
    const menuItemsContainer = document.querySelector('.menu-items');

    tournaments.forEach((tournament) => {
        const tournamentItem = document.createElement('div');
        tournamentItem.classList.add('tournament-item');
        tournamentItem.textContent = tournament.name;

        const seasonsList = document.createElement('div');
        seasonsList.classList.add('seasons-list');
        seasonsList.style.backgroundColor = '#e5e5e5'; // Set the desired background color here

        tournament.seasons.forEach((season) => {
            const seasonItem = document.createElement('div');
            seasonItem.classList.add('season-item');
            seasonItem.textContent = season;
            seasonItem.addEventListener('click', () => {
                console.log('Clicked on season:', season);
            });
            seasonsList.appendChild(seasonItem);
        });

        tournamentItem.addEventListener('click', () => {
            toggleSeasonsList(seasonsList);
        });

        tournamentItem.appendChild(seasonsList);
        menuItemsContainer.appendChild(tournamentItem);
    });
});
