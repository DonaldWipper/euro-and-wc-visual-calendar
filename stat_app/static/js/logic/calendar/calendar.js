function getDates() {
    let dates = [];
    for (let i = 0; i < visualDonatData.length; i++) {
        let cur_group = visualDonatData[i]["id_group"];
        if (cur_group == 1) {
            let dateStr = visualDonatData[i]['time'];
            dates.push(new Date(dateStr));
        }
    }
    return dates;
}

// Функция для преобразования даты в строку формата 'YYYY-MM-DD'
function formatDateToString(date) {
    return date.toISOString().split('T')[0];
}

function getMonthName(date) {
    return monthNames[date.getMonth()];
}

const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

function clear_calendar() {
    let class_elems = ['calendar_elems']
    for (let name in class_elems) {
        d3.selectAll("." + class_elems[name]).remove();
        d3.selectAll("." + class_elems[name]).empty();
    }
}

function calculateWeekNumber(date, startDateCalendar) {
    return d3.timeWeek.count(startDateCalendar, date);
}

function addOneDayToDate(date) {
    let date_new = new Date(date);
    date_new.setDate(date.getDate() + 1);
    return date_new;
}

function subOneDayToDate(date) {
    let date_new = new Date(date);
    date_new.setDate(date.getDate() - 1);
    return date_new;
}


function create_calendar() {
    const month_strings = [];
    let dates = getDates();

    let dateStrings = dates.map(formatDateToString);


    const maxDate = new Date(Math.max(...dates));
    const minDate = new Date(Math.min(...dates));


    const startDateCalendar = new Date(minDate.getFullYear(), minDate.getMonth(), 1);
    const endDateCalendar1 = new Date(maxDate.getFullYear(), maxDate.getMonth(), 1);
    const endDateCalendar = new Date(maxDate.getFullYear(), maxDate.getMonth() + 1, 1);

    d3.timeMonths(startDateCalendar, endDateCalendar).forEach(date => {
        month_strings.push(getMonthName(date));
    });
    number_weeks = d3.timeWeeks(startDateCalendar, endDateCalendar).length

    // var cellSize = 2 * innerRadius / (number_weeks + 2);
    // let shift_x =  (cellSize * 7) / 2;
    // let shift_y =  (cellSize * number_weeks) / 2;


    const aspectRatio = [7, number_weeks + 2]; // Соотношение сторон 3:2

    const dimensions = calculateRectangleDimensions(innerRadius, aspectRatio);
    // console.log(`Width: ${dimensions.width}`);
    // console.log(`Height: ${dimensions.height}`);

    var cellSize = dimensions.height / (number_weeks + 2);
    let shift_x =  dimensions.width / 2;
    let shift_y =  dimensions.height / 2 - cellSize;


    // Add day names
    dayNames.forEach((day, i) => {
        svg.append("text")
            .attr("transform", `translate(${(i + 0.5) * (cellSize ) - shift_x}, ${- shift_y - cellSize / 3})rotate(-45)`)
            .attr("font-family", "sans-serif")
            .attr("class", "calendar_elems")
            .attr("font-size", cellSize / 4)
            .attr("font-weight", "bold")
            .attr("fill", SLICE_COLORS.date)
            .attr("text-anchor", "middle")
            .text(day);
    });

    month_strings.forEach((month, i) => {
        svg.append("text")
            .attr("transform", `translate(${-cellSize/4 - shift_x}, ${(cellSize * (2.5 + i * 5)) - shift_y}) rotate(-90)`)
            .attr("font-family", "sans-serif")
            .attr("class", "calendar_elems")
            .attr("font-size", cellSize / 3)
            .attr("text-anchor", "middle")
            .attr("fill", SLICE_COLORS.date)
            .attr("font-weight", "bold")
            .text(month);
    });




    g2 = svg.append("g")
        .attr("fill", "black")
        .attr("stroke", "#d2d4d8")
        .attr("class", "calendar_elems")
        .selectAll("g")
        .data(function (d) {
            return d3.timeDays(minDate, addOneDayToDate(maxDate));
        })
        .enter()
        .append("g")
        .attr("transform", function (d) {
            d = subOneDayToDate(d)
            const x = (d.getDay()) * cellSize - shift_x,
                y = calculateWeekNumber(d, startDateCalendar) * cellSize - shift_y;

            return "translate(" + x + "," + y + ")";

        });

    g2.append("rect")
        .attr("width", cellSize)
        .attr("height", cellSize)
        .attr("fill", function (d) {
                let dateStr = formatDateToString(d);
                return dateStrings.includes(dateStr) ? SLICE_COLORS.date : "none";

            }
        )

        .attr("opacity", 0.5)
        .attr("class", "calendar_elems")
        .attr("rx", 4)
        .attr("ry", 4)
        .on("mouseover", function () {

            d3.select(this).style("fill", BACK_RECT_COLOR_ON_CLICK);

        })
        .on("mouseout", function () {
            d3.select(this).style("fill", SLICE_COLORS.date);

        })
        .datum(d3.timeFormat("%Y-%m-%d"));

    let g = svg.append("g")
        .attr("fill", "black")
        .attr("stroke", UPPER_RECT_COLOR)
        .attr("class", "calendar_elems")
        .selectAll("g")
        .data(function (d) {
            return d3.timeDays(startDateCalendar, endDateCalendar);
        })
        .enter()
        .append("g")
        .attr("transform", function (d) {

            var x = (d.getDay()) * cellSize - shift_x, // день недели на размер ячейки
                y = calculateWeekNumber(d, startDateCalendar) * cellSize - shift_y;
            return "translate(" + x + "," + y + ")";
        });

    g.append("rect")
        .attr("width", cellSize)
        .attr("height", cellSize)
        .attr("fill", "none")
        .attr("class", "calendar_elems")
        .attr("rx", 4)
        .attr("ry", 4)


    g.append("text")
        .text(function (d) {
            return d.getDate();
        })
        .attr("fill", "none")
        .attr("stroke", function (d) {
                let dateStr = formatDateToString(addOneDayToDate(d));
                return dateStrings.includes(dateStr) ? "white" : UPPER_RECT_COLOR;

            }
        )

        .attr("y", cellSize / 2)
        .attr("x", cellSize / 2)
        .attr("text-anchor", "middle")
        .attr("dominant-baseline", "middle")
        .attr("class", "calendar_elems")
        .style("font-family", "arial")
        .style("font-size", cellSize / 3)

    svg.append("g")
        .attr("fill", "none")
        .attr("class", "calendar_elems")
        .attr("stroke", "#000")
        .selectAll("path")
        .data(function (d) {
            return d3.timeMonths(startDateCalendar, endDateCalendar);
        })
        .enter().append("path")
        .attr("d", pathMonth);

    function pathMonth(t0) {
        var t1 = new Date(t0.getFullYear(), t0.getMonth() + 1, 0),
            d0 = t0.getDay(),
            w0 = calculateWeekNumber(t0, startDateCalendar),
            d1 = t1.getDay(),
            w1 = calculateWeekNumber(t1, startDateCalendar);


        return "M" + (d0 * cellSize - shift_x) + "," + ((w0) * cellSize - shift_y)
            + "H" + (7 * cellSize - shift_x)
            + "V" + ((w1) * cellSize - shift_y)
            + "H" + ((d1 + 1) * cellSize - shift_x)
            + "V" + ((w1 + 1) * cellSize - shift_y)
            + "H" + (0 - shift_x)
            + "V" + ((w0 + 1) * cellSize - shift_y)
            + "H" + (d0 * cellSize - shift_x) + "Z";
    }


}