function getTeams(visualDonatData) {
    let teams = [];
    let country_codes = [];
    let cur_group;
    for (let i = 0; i < visualDonatData.length; i++) {
        cur_group = visualDonatData[i]["id_group"];
        if (cur_group == 0) {
            teams.push(visualDonatData[i])
            country_codes.push(visualDonatData[i]['short_name']);
        }
    }
    return {teams: teams, country_codes: country_codes};
}


function clear_world_map() {
    let class_elems = ['world_map_elems']
    for (let name in class_elems) {
        d3.selectAll("." + class_elems[name]).remove();
        d3.selectAll("." + class_elems[name]).empty();
    }
}


function show_map() {
    clear_all_elems()
    let country_codes = getTeams(visualDonatData).country_codes;


    var format = d3.format(",");

    // Set tooltips
    var tip = d3.tip()
        .attr('class', 'world_map_elems')
        .attr("id", "tip")
        .offset([0, 0])
        .html(function (d) {
            return "<strong></strong><span class='details'>" + d.id + "<br></span>";
        })

    map_width = innerRadius;
    map_height = innerRadius;

    var color = d3.scaleThreshold()

    var path = d3.geoPath();

    svg.append("svg")
        .attr("width", map_width)
        .attr("height", map_height)
        .attr("class", "world_map_elems")
        .attr("id", "world_map_global")
        .append('g')
        .attr('class', 'map');

    var projection = d3.geoMercator()
        .scale(map_width / 4)
        // .scale(960/Math.PI/2)
        .translate([0, map_height / 4]);


    var path = d3.geoPath().projection(projection);


    svg.call(tip);


    svg.append("g")
        .attr("class", "countries")
        .selectAll("path")
        .data(data_world.features)
        .enter().append("path")
        .attr("d", path)
        .attr("class", "world_map_elems")
        .attr("id", function (d, i) {
            return "world_map" + i;
        })
        .style("fill", function (d) {
            if (country_codes.includes(d.id)) {
                return SLICE_COLORS.teams
            } else {
                return BACK_RECT_COLOR;
            }
        })
        .style('stroke', 'white')
        .style('stroke-width', 1.5)
        .style("opacity", 0.8)
        // tooltips
        .style("stroke", "white")
        .style('stroke-width', 0.3)
        .on('mouseover', function (d) {

            if (country_codes.includes(d.id)) {
                tip.show(d);
            }
            d3.select(this)
                .style("opacity", 1)
                .style("stroke", "white")
                .style("stroke-width", 3);
        })
        .on('mouseout', function (d) {
            // tip.hide(d);

            d3.select(this)
                .style("opacity", 0.8)
                .style("stroke", "white")
                .style("stroke-width", 0.3);
        })

    ;

    svg.append("path")
        .datum(topojson.mesh(data_world.features, function (a, b) {
            return a.id !== b.id;
        }))
        // .datum(topojson.mesh(data_worlds.features, function(a, b) { return a !== b; }))
        .attr("class", "names")
        .attr("d", path);

}