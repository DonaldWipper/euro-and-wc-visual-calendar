



const ellipse_koefX = 1.2;
const ellipse_koefY = 1.0;
const global_arc_koefX = ellipse_koefX * 1.05;
const global_arc_koefY = ellipse_koefY * 1.05;

const screenWidth = window.innerWidth;
const screenHeight = window.innerHeight;

const margin = {
    left: 0,
    top: 0,
    right: 0,
    bottom: 0
};

const width = screenWidth - margin.left - margin.right;
const height = screenHeight - margin.top - margin.bottom;

//параметры отрисовки
var outerRadius = Math.min(height / (2 * global_arc_koefY), width / (2 * ellipse_koefX * global_arc_koefX))
var innerRadius = outerRadius * 0.75

var out_arcs = []

var fontSizeInnerSlice = 1 / 5.0 * (outerRadius - innerRadius)
var fontSizeOutterSlice = (global_arc_koefX - 1) * innerRadius * 1 / 5
var fontSizeInside = 1 / 7.2 * (outerRadius - innerRadius)


// Данные внутри арок
//Коэффициент для смещения текста по высоте в дольке
const COEFF_TEXT_IN_SLICE  = {
    teams: 0.8,
    stadium: 0.2,
    city: 3.5,
    date: 2.5,
    stage: 1
};


//ШРИФТА

// Шрифт внутри бублика
const FONT_FAMILY_DONUT = "bellota_bold"  //шрифт внутри бублика
const FONT_FAMILY_PLAYOFF =  "bellota_bold"
const FONT_FAMILY_SCORE = "bellota_bold"  //шрифт внутри появляющегося счете

//Шрифт для selected_list
const FONT_FAMILY_SELECTED_LIST = "bellota"  //шрифт внутри бублика

//цвета
const click_color = "#debcd1";     //ЦВЕТ, когда кликаем на дольки
const STROKE_COLOR_SLICE = "black"; //ЦВЕТ обводки внутренней дольки

//Цвет долек
const SLICE_DEFAULT_COLOR = "#E0E0BA"
const SLICE_ON_CLICK_COLOR = "#332D18"




const SLICE_COLORS = {
    teams: "#8EDF00",
    stadium: "#00A4D8",
    date: "#FD0013",
    stage: "#FF9200"
}


// function get_country(tournamentId) {
//     switch (tournamentId) {
//         case(500):
//             return ['Qatar']
//         case(467):
//             return ['Russia']
//         case(1472):
//             return ['France']
//         case(1476):
//             return [
//                 "Russia",
//                 "Azerbaijan",
//                 "Denmark",
//                 "England",
//                 "Germany",
//                 "Hungary",
//                 "Italy",
//                 "Netherlands",
//                 "Romania",
//                 "Scotland",
//                 "Spain",
//                 "Sweden"
//             ]
//         case(406):
//             return ['Brazil']
//         case(369):
//             return ['France']
//         case(1468):
//             return ['Ukraine']
//         case(1460):
//             return ['Portugal']
//         case(1456):
//             return ['Belgium']
//         case(1448):
//             return ['Sweden']
//         case(1436):
//             return ['Italy']
//         case(1424):
//             return ['Italy']
//         case(1416):
//             return ['France']
//         case(365):
//             return ['South Africa']
//         case(366):
//             return ['Germany']
//
//     }
// }


const STAD_MAP_SCALE = {
    500: {center:[51.3, 25.1], scale:innerRadius * 50},
    467: {center:[100, 60], scale:innerRadius * 0.7},
    369: {center:[2, 47], scale:innerRadius * 5},
    1416: {center:[2, 47], scale:innerRadius * 5},
    1472: {center:[2, 47], scale:innerRadius * 5},
    406: {center:[-53, -14], scale:innerRadius * 2},
    365: {center:[24, -28], scale:innerRadius * 3.5},
    1468: {center:[50, 30], scale:innerRadius * 2},
}

//Шрифт внутри долек
const TEAM_COLOR = "#6C7E1E"; //ЦВЕТ на дольках с командами
const СITY_COLOR = "#009DA4"; //ЦВЕТ город
const DATE_NUMBER_COLOR = "#FD0013"; //ЦВЕТ числа в дате
const MONTH_COLOR = "#B6B174"
const STAGE_COLOR = "#FF9200"
const STADIUM_COLOR = "#B6B174"

//Внешие арки
const OUT_COLOR = "white";
const OUT_COEFF_TEXT_IN_SLICE = 3

const OUT_ARC_COLORS = {
    teams: "url(#gradientGreen)",
    stadium: "url(#gradientBlue)",
    date: "url(#gradientRed)",
    stage: "url(#gradientOrange)"
}


//Cмещение теста в арке от края
const START_OFFSET_STADIUM = 2;
const START_OFFSET_TEAM = 3;

//Доп информация в полуарке сверху
const COEFF_INNER_RADIUS = 1.0 / 2;
const INFO_RECT_UP = innerRadius * COEFF_INNER_RADIUS;     // координата Y верхней грани четырехугольника


//Счет
const BACK_RECT_COLOR = "#333121";
const BACK_RECT_COLOR_ON_CLICK = "#2C3901"
const SCORE_RECT_COLOR = "#15130F";
const SCORE_TEAM_COLOR = "#C3B06A";
const UPPER_RECT_COLOR = "#847C5B"
const HAT_RECT_COLOR_LEFT = "#F0E59E"
const HAT_RECT_COLOR_RIGHT = "#998B54"
const SCORE_HEIGTH = innerRadius / 8;

const FONT_FAMILY_SCORE_COLOR = "#F0E59E";

const SPACE_SCORE_SIZE_COEFF = 1/ 2;
const SCORE_WIDTH =  2 * innerRadius

//центральный четырехугольник, где выводится счет зависит от SCORE_HEIGTH  параметра
const CENTRE_SCORE_WIDTH_COEFF = 1 / 4.0;
const CENTRE_SCORE_SHIFT_COEFF = 1 / 2;
const CENTRE_SCORE_HEIGHT_COEFF =  2.0 / 3;

//Четырехугольник шапка
const HAT_SCORE_WIDTH_COEFF = 1 / 2.2;
const HAT_SCORE_HEIGHT_COEFF =  2.0 / 3;

//Изображение команды
const IMAGE_COEFF = 1 / 1.2;
const IMAGE_POSITION_X_COEFF = 0.75;
const IMAGE_POSITION_Y_COEFF = 1 / 2.0;

//Текст
const TEXT_TEAM_COEFF = 0.4;
const TEXT_SCORE_COEFF = 2 /3.0;
const UPPER_RECT_COEFF = 0.4;
const HAT_TEXT_SCORE_COEFF = 7 / 10
//Аниммация
const ANIMATION_DURATION = 0;


//Плейофф и группы
const TABLEAU_COEFF = 0.4;
const TABLEAU_UP_Y = innerRadius * TABLEAU_COEFF;

const PLAYOFF_COEFF = 0.6;
const GROUPS_COEFF = 0.3;

const COLOR_GROUP = "#847C5B";

// const PLAYOFF_COEFF = 0.7
// const PLAYOFF_WIDTH =  PLAYOFF_COEFF * innerRadius;
// const SPACE_TO_WIDTH_COEFF =  0.2
// const columnWidth = PLAYOFF_WIDTH / ((1 + SPACE_TO_WIDTH_COEFF) * 4)
// const NODE_SPACING = columnWidth * SPACE_TO_WIDTH_COEFF
//
//
//
//
//
//
//
// const GROUP_RECT_COLOR = "#847C5B"