
var championships = [
    {
        id: 378,
        name: "World Cup 1930 Uruguay",
        countries: ["Uruguay"],
        center: [-55, -32],
        scale:innerRadius * 5
    },
    {
        id: 377,
        name: "World Cup 1934 Italy",
        countries: ["Italy"],
        center: [12, 42],
        scale: innerRadius * 5
    },
    {
        id: 376,
        name: "World Cup 1938 France",
        countries: ["France"],
        center: [2.2137, 46.2276],
        scale:innerRadius * 5
    },
    {
        id: 379,
        name: "World Cup 1950 Brazil",
        countries: ["Brazil"],
        center: [-51.9253, -14.235],
        scale:innerRadius * 2.5
    },
    {
        id: 382,
        name: "World Cup 1954 Switzerland",
        countries: ["Switzerland"],
        center: [8.2275, 46.8182],
        scale:innerRadius * 5
    },
    {
        id: 374,
        name: "World Cup 1958 Sweden",
        countries: ["Sweden"],
        center: [18.6435, 60.1282],
        scale:innerRadius * 5
    },
    {
        id: 380,
        name: "World Cup 1962 Chile",
        countries: ["Chile"],
        center: [-71.5375, -35.6751],
        scale:innerRadius * 2.5
    },
    {
        id: 407,
        name: "World Cup 1966 England",
        countries: ["England"],
        center: [-0.1276, 53.5074],
        scale:innerRadius * 5
    },
    {
        id: 409,
        name: "World Cup 1970 Mexico",
        countries: ["Mexico"],
        center: [-99.1332, 19.4326],
        scale:innerRadius * 5
    },
    {
        id: 375,
        name: "World Cup 1974 Germany",
        countries: ["Germany"],
        center: [10.4515, 51.1657],
        scale:innerRadius * 5
    },
    {
        id: 381,
        name: "World Cup 1978 Argentina",
        countries: ["Argentina"],
        center: [-58.4173, -34.6118],
        scale:innerRadius * 2
    },
    {
        id: 1478,
        name: "FIFA U-20 World Cup Argentina 2023",
        countries: ["Argentina"],
        center: [-58.4173, -38.6118],
        scale:innerRadius * 2
    },
    {
        id: 373,
        name: "World Cup 1982 Spain",
        countries: ["Spain"],
        center: [-3.7492, 40.4637],
        scale:innerRadius * 5
    },
    {
        id: 372,
        name: "World Cup 1986 Mexico",
        countries: ["Mexico"],
        center: [-99.1332, 19.4326],
        scale:innerRadius * 2.5
    },
    {
        id: 371,
        name: "World Cup 1990 Italy",
        countries: ["Italy"],
        center: [12.5674, 41.8719],
        scale:innerRadius * 5
    },
    {
        id: 370,
        name: "World Cup 1994 USA",
        countries: ["United States"],
        center: [-95.7129, 37.0902],
        scale:innerRadius * 5
    },
    {
        id: 369,
        name: "World Cup 1998 France",
        countries: ["France"],
        center: [2.3522, 48.8566],
        scale:innerRadius * 5
    },
    {
        id: 367,
        name: "World Cup 2002 South Korea / Japan",
        countries: ["South Korea", "Japan"],
        center: [127.7669, 35.9078],
        scale:innerRadius * 5
    },
    {
        id: 366,
        name: "World Cup 2006 Germany",
        countries: ["Germany"],
        center: [10.4515, 51.1657],
        scale:innerRadius * 5
    },
    {
        id: 365,
        name: "World Cup 2010 South Africa",
        countries: ["South Africa"],
        center: [24.0297, -29.6099],
        scale:innerRadius * 5
    },
    {
        id: 406,
        name: "World Cup 2014 Brazil",
        countries: ["Brazil"],
        center: [-51.9253, -14.235],
        scale:innerRadius * 2.5
    },
    {
        id: 467,
        name: "World Cup 2018 Russia",
        countries: ["Russia"],
        center: [32.6173, 55.7558],
        scale:innerRadius * 0.5
    },
    {
        id: 1416,
        name: "UEFA European Championship 1960 France",
        countries: ["France"],
        center: [2.2137, 46.2276],
        scale:innerRadius * 5
    },
    {
        id: 1420,
        name: "UEFA European Championship 1964 Spain",
        countries: ["Spain"],
        center: [-3.7492, 40.4637],
        scale:innerRadius * 5
    },
    {
        id: 1424,
        name: "UEFA European Championship 1968 Italy",
        countries: ["Italy"],
        center: [12.5674, 41.8719],
        scale:innerRadius * 5
    },
    {
        id: 1428,
        name: "UEFA European Championship 1972 Belgium",
        countries: ["Belgium"],
        center: [4.4699, 50.5039],
        scale:innerRadius * 5
    },
    {
        id: 1432,
        name: "UEFA European Championship 1976 Yugoslavia",
        countries: ["Yugoslavia"],
        center: [20.4656, 44.7866],
        scale:innerRadius * 5
    },
    {
        id: 1436,
        name: "UEFA European Championship 1980 Italy",
        countries: ["Italy"],
        center: [12.5674, 41.8719],
        scale:innerRadius * 5
    },
    {
        id: 1440,
        name: "UEFA European Championship 1984 France",
        countries: ["France"],
        center: [2.2137, 46.2276],
        scale:innerRadius * 5
    },
    {
        id: 1444,
        name: "UEFA European Championship 1988 West Germany",
        countries: ["West Germany"],
        center: [10.4515, 51.1657],
        scale:innerRadius * 5
    },
    {
        id: 1448,
        name: "UEFA European Championship 1992 Sweden",
        countries: ["Sweden"],
        center: [18.6435, 60.1282],
        scale:innerRadius * 5
    },
    {
        id: 1452,
        name: "UEFA European Championship 1996 England",
        countries: ["England"],
        center: [-0.1276, 51.5074],
        scale:innerRadius * 5
    },
    {
        id: 1456,
        name: "UEFA European Championship 2000 Belgium-Netherlands",
        countries: ["Belgium", "Netherlands"],
        center: [4.4699, 50.5039],
        scale:innerRadius * 5
    },
    {
        id: 1460,
        name: "UEFA European Championship 2004 Portugal",
        countries: ["Portugal"],
        center: [-8.2245, 39.3999],
        scale:innerRadius * 5
    },
    {
        id: 1464,
        name: "UEFA European Championship 2008 Austria-Switzerland",
        countries: ["Austria", "Switzerland"],
        center: [14.5501, 47.5162],
        scale:innerRadius * 5
    },
    {
        id: 1468,
        name: "UEFA European Championship 2012 Poland-Ukraine",
        countries: ["Poland", "Ukraine"],
        center: [19.1451, 51.9194],
        scale:innerRadius * 5
    },
    {
        id: 1472,
        name: "UEFA European Championship 2016 France",
        countries: ["France"],
        center: [2.2137, 46.2276],
        scale:innerRadius * 5
    },
    {
        id: 1476,
        name: "UEFA European Championship 2020",
        countries: [
            "Russia",
            "Azerbaijan",
            "Denmark",
            "England",
            "Germany",
            "Hungary",
            "Italy",
            "Netherlands",
            "Romania",
            "Scotland",
            "Spain",
            "Sweden"
        ],
        center: [10.4515, 51.1657],
        scale:innerRadius
    },
    {
        id: 500,
        name: "World Cup 2022 Qatar",
        countries: ["Qatar"],
        center: [51.1839, 25.3548],
        scale:innerRadius * 50
    },
    {
        "id": 1479,
        "name": "Europe",
        "countries": [
            "Albania",
            "Andorra",
            "Austria",
            "Belarus",
            "Belgium",
            "Bosnia and Herzegovina",
            "Bulgaria",
            "Croatia",
            "Cyprus",
            "Czech Republic",
            // "Denmark",
            "Estonia",
            "Finland",
            "France",
            "Germany",
            "Greece",
            "Hungary",
            "Iceland",
            "Ireland",
            "Italy",
            "Kosovo",
            "Latvia",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Moldova",
            "Monaco",
            "Montenegro",
            "Netherlands",
            "North Macedonia",
            "Norway",
            "Poland",
            "Portugal",
            "Romania",
            "San Marino",
            "Serbia",
            "Slovakia",
            "Slovenia",
            "Spain",
            "Sweden",
            "Switzerland",
            "Ukraine",
            "United Kingdom",
            "Vatican City"
        ],
        "center": [10.4515, 53.1657],
        "scale": innerRadius * 1.3
    },
    {
        id: 1480,
        name: "UEFA European Championship 2024 Germany",
        countries: ["Germany"],
        center: [10.4515, 51.1657],
        scale: innerRadius * 5
    }
];
