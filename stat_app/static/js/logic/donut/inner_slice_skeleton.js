//каркас для внутренних долек бублика

//Возращает

// textArc - траектория текста внизу дольки
// textArcCentre - траектория текста
// ellipseInnerArc - внутренняя арка ближе к центру
// imageWidth - размер картинки внутри арки
// firstSmallLoc - координаты точки на внутренней линии дольки
// angle - угол поворота для смещения картинки относительно firstSmallLoc



//параметр, который задает каждую арку
const arcOut = {
    firstLoc: "",
    radLoc: "",
    secLoc: "",
    firstSmallLoc: "",
    secSmallLoc: "",
    radSmallLoc: ""
};

function generate_skeleton(elem, d, i, part_bounces) {

    const firstDotOuterEx = /M(.*?)A/;
    const radOuterEx = /A(.*?)0 0,1/;
    const secondDotOuterEx = /0 0,1(.*?)L/;
    const firstInnerDotEx = /L(.*?)A/;
    const radSmallElEx = /A(.*?)0 0,0/;
    const secondSmallDotEx = /0 0,0(.*?)Z/;

    let firstLoc = firstDotOuterEx.exec(elem)[1]
    let radLoc = radOuterEx.exec(elem)[1]

    let secLoc = secondDotOuterEx.exec(elem)[1]
    let firstSmallLoc = firstInnerDotEx.exec(elem)[1]


    let secSmallLoc = secondSmallDotEx.exec(elem)[1]
    let radSmallLoc = radSmallElEx.exec(elem)[1]

    radSmallLoc = radSmallElEx.exec(radSmallLoc + "0 0,0")[1]


    const first = firstLoc.split(",");
    const second = secLoc.split(",");
    const rad = radLoc.split(",");

    //точки и радиус для внутренней арки
    let newFirst = getEllipseCoords(parseFloat(first[0]), parseFloat(first[1]), ellipse_koefX, ellipse_koefY, outerRadius).join(",")
    let newLast = getEllipseCoords(parseFloat(second[0]), parseFloat(second[1]), ellipse_koefX, ellipse_koefY, outerRadius).join(",")
    let newRad = [rad[0] * ellipse_koefX, rad[1] * ellipse_koefY].join(",")

    //точки для внешней арки
    let newFirstOut = getEllipseCoords(parseFloat(first[0]), parseFloat(first[1]), global_arc_koefX, global_arc_koefY, outerRadius).join(",")
    let newSecOut = getEllipseCoords(parseFloat(second[0]), parseFloat(second[1]), global_arc_koefX, global_arc_koefY, outerRadius).join(",")


    let FirstPath = "M" + newFirst + "A" + newRad + " 0 0, 1" + newLast
    let ellipseInnerArc = FirstPath + "L" + firstSmallLoc + "A" + radSmallLoc + "0 0,0" + secSmallLoc + "Z"

    let imageWidth = getDistance(firstSmallLoc.split(","), secSmallLoc.split(","))
    let path_length_no_image = getDistance(secSmallLoc.split(","), newFirst.split(","))

    // imageWidth = fit_image_size(imageWidth, path_length_no_image)

    let coeff = 0.4;
    //центры долек, чтобы писать в центре
    switch (d.data.id_group) {
        case('0'):
            coeff = COEFF_TEXT_IN_SLICE.teams;
            break;
        case("1"):
            coeff = COEFF_TEXT_IN_SLICE.date;
            break;
        case("2"):
            coeff = COEFF_TEXT_IN_SLICE.city;
            break;
        case("3"):
            coeff = COEFF_TEXT_IN_SLICE.stage;
            break;
    }

    let centreSec = getPointDivided(newLast.split(","), newFirst.split(","), coeff).join(",")
    let centreFirst = getPointDivided(firstSmallLoc.split(","), secSmallLoc.split(","), coeff).join(",")

    let centre_path_length = getDistance(centreFirst.split(","), centreSec.split(","))

    let k = imageWidth * 1.0 / (centre_path_length - imageWidth) //коэффицент пути
    let nextPointCentre = getPointDivided(centreFirst.split(","), centreSec.split(","), k).join(",")

    let k2 = imageWidth * 1.0 / (path_length_no_image - imageWidth)
    let nextPointToImage = getPointDivided(newFirst.split(","), secSmallLoc.split(","), 1 / k2).join(",")

    let textArc;
    let textArcCentre;

    let newLast_ = newLast;
    let firstSmallLoc_ = firstSmallLoc;

    if (d.data.id_group == 2) {
        coeff = COEFF_TEXT_IN_SLICE.stadium
        newLast_ = getPointDivided(newLast.split(","), newFirst.split(","), coeff).join(",")
        firstSmallLoc_ = getPointDivided(firstSmallLoc.split(","), secSmallLoc.split(","), coeff).join(",")
    }

    if (d.data.id_group == 0) {
        textArc = "M" + newFirst + "L" + nextPointToImage + "Z";
        textArcCentre = "M" + centreSec + "L" + nextPointCentre + "Z";
    } else if ((d.data.id_group == 3) &&  d.data.name.toLowerCase().includes('group')) { //разные направления пути для разных видов долек
        textArcCentre = "M" + centreSec + "L" + centreFirst + "Z";
    } else {
        textArc = "M" + newLast_ + "L" + firstSmallLoc_ + "Z";
        textArcCentre = "M " + centreFirst + " L" + centreSec + "Z";
    }

    let newRadOut = [global_arc_koefX * rad[0], global_arc_koefY * rad[1]].join(",")

    let angle = getRotateAngle(firstSmallLoc.split(","), secSmallLoc.split(",")) //угол поворота для смещения картинки

    //если текущий кусок самый крайний в группе
    let cur_group = visualDonatData[i]["id_group"]
    if (cur_group != -1) {
        if (part_bounces[cur_group][0] === i) {
            let a = Object.assign({}, arcOut);
            a["firstLoc"] = newFirstOut
            a["radLoc"] = newRadOut
            a["firstSmallLoc"] = newFirst
            a["radSmallLoc"] = newRad
            out_arcs.push(a)
        }
        if (part_bounces[cur_group][1] == i) {
            out_arcs[cur_group]["secLoc"] = newSecOut
            out_arcs[cur_group]["secSmallLoc"] = newLast
        }
    }

    return [textArc,
        textArcCentre,
        ellipseInnerArc,
        imageWidth,
        firstSmallLoc,
        angle
    ]

}