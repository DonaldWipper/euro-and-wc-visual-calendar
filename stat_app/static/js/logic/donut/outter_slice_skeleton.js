function get_arc_color(i) {
    switch (i) {
        case(0):
            return OUT_ARC_COLORS.teams;
        case(1):
            return OUT_ARC_COLORS.date;
        case(2):
            return OUT_ARC_COLORS.stadium;
        case(3):
            return OUT_ARC_COLORS.stage;
    }
}
function generate_outter_skeleton() {
    let pathOutArc;
    let pathText;
    let textArcCentreBack;
    let textArcCentre;
    for (let i = 0; i < out_arcs.length; i++) {

        pathOutArc = "M" + out_arcs[i]["firstLoc"] +
            "A" + out_arcs[i]["radLoc"] +
            " 0 0, 1" + out_arcs[i]["secLoc"] +
            "L" + out_arcs[i]["secSmallLoc"] +
            "A" + out_arcs[i]["radSmallLoc"] +
            " 0 0,0" + out_arcs[i]["firstSmallLoc"] + "Z"

        pathText = "M" + out_arcs[i]["firstSmallLoc"] +
            "A" + out_arcs[i]["radSmallLoc"] +
            " 0 0,1" + out_arcs[i]["secSmallLoc"] + "Z"


        let circle_rad = Math.abs(out_arcs[i]["radLoc"].split(",")[1] - out_arcs[i]["radSmallLoc"].split(",")[1]) / 1.5


        let centreRad = getPointDivided(out_arcs[i]["radLoc"].split(","), out_arcs[i]["radSmallLoc"].split(","), OUT_COEFF_TEXT_IN_SLICE).join(",")
        let centreSec = getPointDivided(out_arcs[i]["firstLoc"].split(","), out_arcs[i]["firstSmallLoc"].split(","), OUT_COEFF_TEXT_IN_SLICE).join(",")
        let centreFirst = getPointDivided(out_arcs[i]["secLoc"].split(","), out_arcs[i]["secSmallLoc"].split(","), OUT_COEFF_TEXT_IN_SLICE).join(",")

        let centreRadBack = getPointDivided(out_arcs[i]["radLoc"].split(","), out_arcs[i]["radSmallLoc"].split(","), 0.5).join(",")
        let centreSecBack = getPointDivided(out_arcs[i]["firstLoc"].split(","), out_arcs[i]["firstSmallLoc"].split(","), 0.5).join(",")
        let centreFirstBack = getPointDivided(out_arcs[i]["secLoc"].split(","), out_arcs[i]["secSmallLoc"].split(","), 0.5).join(",")


        textArcCentre = "M" + centreSec +
            "A" + centreRad +
            " 0 0,1" + centreFirst + "Z";

        textArcCentreBack = "M" + centreFirstBack +
            "A" + centreRadBack +
            " 0 0,0" + centreSecBack + "Z";


        svg.append("path")
            .attr("class", "outArc")
            .attr("id", "outArc" + i)
            .attr("d", pathOutArc)
            .style("opacity", 0.7)
            .style("fill", function () {
                return get_arc_color(i)
            })
            .on("mouseover", function () {
                d3.select(this).style("fill", SLICE_ON_CLICK_COLOR);
                clear_all_elems();
                switch (i) {
                    case(0):
                        show_map();
                        return
                    case(1):
                        create_calendar();

                        return
                    case(2):
                        create_map();
                        return
                    case (3):
                        show_playoff();
                        draw_groups(games);
                        return;
                }


            })
            .on("mouseout", function () {
                d3.select(this).style("fill", get_arc_color(i));
            });


        svg.append("path")
            .attr("class", "outArcPath")
            .attr("id", "outArcPath" + i)
            .attr("d", function () {
                if (i != 3) {
                    return textArcCentre
                } else {
                    return  textArcCentreBack
                }
            })
            .attr("fill", "none")
            .attr("stroke-width", 0)

        svg.append("text")
            .style("font-size", fontSizeOutterSlice + "px")
            .append("textPath")
            .attr("startOffset", function () {
                if (i != 3) {
                    return "15%";
                } else {
                    return "15%";
                }

            })
            .attr("id", "outArcText" + i)
            //.attr("dy", -fontSizeOutterSlice / 4 + "px")
            .attr("pointer-events", "none")
            .attr("xlink:href", "#outArcPath" + i)
            .text(outArcsData[i]["name"])
            .style("font-family", FONT_FAMILY_DONUT)
            .style("font-weight", "bold")
            .style("fill", OUT_COLOR)

        //  - 100
        // svg.append("circle")
        //     .attr("id", "circle" + i)
        //     .attr("cx", out_arcs[i]["firstLoc"].split(",")[0] - 10)
        //     .attr("cy", out_arcs[i]["firstLoc"].split(",")[1] - 40)
        //     .attr("r", circle_rad)
        //     .style("fill", function () {
        //         return get_arc_color(i)
        //
        //     })
        //
        // svg.append("image")
        //     .attr("width",
        //         circle_rad)
        //     .attr("height", circle_rad)
        //     .attr("x", out_arcs[i]["firstLoc"].split(",")[0])
        //     .attr("y", out_arcs[i]["firstLoc"].split(",")[1])
        //     .attr("id", "image_circle" + i)
        //     .attr("xlink:href", function () {

        // });
        // .attr("transform", function () {
        //     a = 180 + angle
        //     if (visualDonatData[i]["id_group"] == 0) {
        //         //a = angle
        //         return "rotate(" + a + ", " + firstSmallLoc.split(",")[0] + " ," + firstSmallLoc.split(",")[1] + ")" +
        //             "translate(-" + imageWidth + ",0)"
        //     } else {
        //         return ""
        //     }
        //
        // })
        // .on("mouseover", function () {
        //     if (['0'].includes(d.data.id_group)) {
        //         clear_all_elems();
        //         draw_score(function_get_games_array(show_games[i]), Object.keys(games[0]));
        //         restore_colors()
        //         d3.select("#ArcEllipse" + i).style("fill", SLICE_ON_CLICK_COLOR);
        //         setClicked(i, click_events[i]);
        //     }
        //
        // })
        // .on("mouseout", function (d, i) {
        //     restore_colors()
        //     //d3.select(this).style("fill", visualDonatData[i]["color"]);
        // })
        //
        // .attr("xlink:href", function () {
        //     //console.log("")
        //     if (visualDonatData[i]["id_group"] == 0) {
        //         return visualDonatData[i]["image"]
        //     }
        // });


        // svg.append("circle")
        //     .attr("cx", 150)
        //     .attr("cy", 30)
        //     .attr("r", 20);
    }
}
