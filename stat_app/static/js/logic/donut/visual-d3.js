// список долек к каждой из 4-х зон
var part_bounces = {
    "0": [10000, -1],
    "1": [10000, -1],
    "2": [10000, -1],
    "3": [10000, -1]
}

function text_centre_color(id_group, name) {
    switch (id_group) {
        case("0"):
            return TEAM_COLOR;
        case("1"):
            return DATE_NUMBER_COLOR;
        case("2"):
            return СITY_COLOR;
        case("3"):
            if (!name.toLowerCase().includes('group')) {
                return STAGE_COLOR;
            } else {
                return STADIUM_COLOR;
            }

    }
}

function text_centre_second(id_group, name) {
    switch (id_group) {
        case("0"):
            return TEAM_COLOR;
        case("1"):
            return MONTH_COLOR;
        case("3"):
            if (name.toLowerCase().includes('group')) {
                return STAGE_COLOR;
            } else {
                return STADIUM_COLOR;
            }
    }
}


function restore_colors() {
    for (let i = 0; i < visualDonatData.length; i++) {
        if (['0', '1', '2', '3'].includes(visualDonatData[i]["id_group"])) {
            d3.select("#ArcEllipse" + i).style("fill", SLICE_DEFAULT_COLOR);
            d3.select("#text_centre" + i).style("fill", text_centre_color(visualDonatData[i]["id_group"], visualDonatData[i]["name"]));
            d3.select("#text_centre_second" + i).style("fill", text_centre_second(visualDonatData[i]["id_group"], visualDonatData[i]["name"]));
        }
    }
}


function restore_colors_outArcs() {
    for (i = 0; i < outArcsData.length; i++) {
        d3.select("#outArc" + i).style("fill", outArcsData[i]["color"]);
    }
}


function setClicked(cur_number, Array) {
    let id_group;
    for (let i = 0; i < Array.length; i++) {

        id_group = visualDonatData[Array[i]]["id_group"]
        switch (id_group) {
            case("0"):
                d3.select("#ArcEllipse" + Array[i]).style("fill", SLICE_COLORS.teams);
                continue;
            case("1"):
                d3.select("#text_centre" + Array[i]).style("fill", "white");
                d3.select("#ArcEllipse" + Array[i]).style("fill", SLICE_COLORS.date);
                continue;
            case("2"):
                d3.select("#text_centre" + Array[i]).style("fill", "white");
                d3.select("#ArcEllipse" + Array[i]).style("fill", SLICE_COLORS.stadium);
                continue;
            case("3"):
                d3.select("#text_centre" + Array[i]).style("fill", "white");
                d3.select("#text_centre_second" + Array[i]).style("fill", "black");
                d3.select("#ArcEllipse" + Array[i]).style("fill", SLICE_COLORS.stage);
                continue;
        }

    }
}

//для каждой части находим первый и последний кусочек
function getPartBounces(visualDonatData, part_bounces) {

    let cur_group_left;
    let cur_group_right;
    let cur_group;

    for (i = 0; i < visualDonatData.length; i++) {

        cur_group = visualDonatData[i]["id_group"];
        if (cur_group == -1) {
            continue;
        }
        cur_group_left = part_bounces[cur_group][0]
        cur_group_right = part_bounces[cur_group][1]

        if (i < cur_group_left) {
            part_bounces[cur_group][0] = i
        }
        if (i > cur_group_right) {
            part_bounces[cur_group][1] = i
        }
    }
}


//рисуем эллипс
function createDonatDiagram(
    visualDonatData,
    pie,
    arc,
    global_arc_koefX, //коэффициенты растяжения круга
    global_arc_koefY,
    outerRadius,
    fontSizeInnerSlice,
    nameElement = "visualDonatArcs", // имя основного элемента
    ArcElement = "visualDonatArc",   // каркас для текстовых элементов
    textArcElement = "visualDonatText"
) // текст, который наносится на каркас
{

    getPartBounces(visualDonatData, part_bounces);

    linear_gradent(SLICE_COLORS.date, color_to = "#FFFFFF", "gradientRed")
    linear_gradent(SLICE_COLORS.stadium, color_to = "#D1DBDD", "gradientBlue")
    linear_gradent(SLICE_COLORS.stage, color_to = "#D1DBDD", "gradientOrange")
    linear_gradent(SLICE_COLORS.teams, color_to = "#D1DBDD", "gradientGreen")

    svg.selectAll("." + nameElement)
        .data(pie(visualDonatData))
        .enter().append("path")
        .attr("class", nameElement)
        .attr("d", arc)
        .attr("id", nameElement)
        .style("fill", function (d, i) {
            return "none";
        })
        .each(function (d, i) {
            const [textArc, textArcCentre, ellipseInnerArc, imageWidth, firstSmallLoc, angle] = generate_skeleton(d3.select(this).attr("d"), d, i, part_bounces);

            // Путь для текста на дне дольки
            svg.append("path")
                .attr("class", "hidden" + ArcElement)
                .attr("id", ArcElement + i)
                .attr("stroke", "blue")
                .attr("stroke-width", 0)
                .attr("d", textArc)

            // Путь для текста в центре дольки
            svg.append("path")
                .attr("class", "hidden" + ArcElement + "Centre")
                .attr("id", ArcElement + "Centre" + i)
                .attr("d", textArcCentre)
                .attr("stroke", "black")
                .attr("stroke-width", 0)
                .style("fill", "black");


            //visualDonatData[i]["color"]
            //Маленькие внутренние дольки

            svg.append("path")
                .attr("class", "slice_ellipse")
                .attr("id", "ArcEllipse" + i)
                .attr("stroke-width", 0)
                .attr("stroke", function () {  //цвет обводки дольки
                    return STROKE_COLOR_SLICE;
                })
                .attr("d", ellipseInnerArc)
                .style("opacity", 0.7)
                //.style("fill", visualDonatData[i]["color"])
                .attr('fill', function () {

                    if (d.data.id_group != -1) {
                        return SLICE_DEFAULT_COLOR;
                    } else {
                        return "none";
                    }
                })
                .on("mouseover", function () {

                    if (['0', '1', '2', '3'].includes(d.data.id_group)) {
                        clear_all_elems();

                        restore_colors()
                        d3.select(this).style("fill", SLICE_ON_CLICK_COLOR);
                        setClicked(i, click_events[i]);
                        clear_all_elems();
                        draw_score(function_get_games_array(show_games[i]), Object.keys(games[0]));
                    }

                })
                .on("mouseout", function (d, i) {
                    // clear_all_elems();
                    restore_colors()
                    //d3.select(this).style("fill", visualDonatData[i]["color"]);
                });


            svg.append("image")
                .attr("width",
                    imageWidth)
                .attr("height", imageWidth)
                .attr("x", firstSmallLoc.split(",")[0])
                .attr("y", firstSmallLoc.split(",")[1])
                .attr("id", "rect" + i)
                .attr("transform", function () {
                    a = 180 + angle
                    if (visualDonatData[i]["id_group"] == 0) {
                        //a = angle
                        return "rotate(" + a + ", " + firstSmallLoc.split(",")[0] + " ," + firstSmallLoc.split(",")[1] + ")" +
                            "translate(-" + imageWidth + ",0)"
                    } else {
                        return ""
                    }

                })
                .on("mouseover", function () {
                    if (['0'].includes(d.data.id_group)) {
                        clear_all_elems();
                        restore_colors()
                        d3.select("#ArcEllipse" + i).style("fill", SLICE_ON_CLICK_COLOR);
                        setClicked(i, click_events[i]);
                        clear_all_elems();
                        draw_score(function_get_games_array(show_games[i]), Object.keys(games[0]));
                    }

                })
                .on("mouseout", function (d, i) {
                    restore_colors()
                    //d3.select(this).style("fill", visualDonatData[i]["color"]);
                })

                .attr("xlink:href", function () {
                    //console.log("")
                    if (visualDonatData[i]["id_group"] == 0) {
                        return visualDonatData[i]["image"]
                    }
                });
            //Append the label names on the outside


        });

    //текста добавляем отдельно. Это все, что в центре


    svg.selectAll("." + textArcElement + "Centre")
        .data(pie(visualDonatData))
        .enter().append("text")
        .style("font-size", fontSizeInnerSlice / 1.7 + "px")
        .style("font-family", FONT_FAMILY_DONUT)
        .style("font-weight", "bold")
        .attr("id", function (d, i) {
            return "text_centre" + i
        })
        .attr("dy", fontSizeInnerSlice / 4)
        .style("fill", function (d, i) {
            return text_centre_color(d.data.id_group, d.data.name)

        })

        .attr("class", ArcElement + "Centre")
        .attr("pointer-events", "none")
        .append("textPath")
        .attr("href", function (d, i) {
            return "#" + ArcElement + "Centre" + i;
        })
        .attr("startOffset", function (d, i) {
            if (d.data.id_group == 0) {
                return 50 - START_OFFSET_TEAM + "%";
            } else if ((d.data.id_group == 3) && d.data.name.toLowerCase().includes('group')) {
                return 50 - START_OFFSET_TEAM + "%";
            } else {
                return START_OFFSET_STADIUM + "%";
            }
        })
        .attr("text-anchor", function (d, i) {
            if ((d.data.id_group == 0) || ((d.data.id_group == 3) && d.data.name.toLowerCase().includes('group'))) {
                return "end";
            } else {
                return "start";
            }
        })
        //.attr("xlink:href",function(d) {return d.data.image;})
        .text(function (d, i) {
            switch (d.data.id_group) {
                case("0"):
                    return d.data.name.toUpperCase();
                case("1"):
                    return d.data.name.split(" ")[0];
                case("2"):
                    return d.data.name.split(";")[1].toUpperCase();
                case("3"):
                    if (d.data.name.toLowerCase().includes('group')) {
                        return "GROUP";
                    } else {
                        return d.data.name.toUpperCase();
                    }
            }

        })
        .each(function (d, i) {
            let place_size =  d3.select("#" + ArcElement + "Centre" + i).node().getTotalLength();
            fit_text_in_rect(place_size, this);
        })
        //второе слово для даты
        .append("tspan")
        .attr("id", function (d, i) {
            return "text_centre_second" + i;
        })
        .style("font-family", FONT_FAMILY_DONUT)
        .style("font-weight", "bold")
        .style("fill", function (d, i) {
            return text_centre_second(d.data.id_group, d.data.name)
        })
        .text(function (d) {
            switch (d.data.id_group) {
                case("1"):
                    return " " + d.data.name.split(" ")[2].toUpperCase();
                case("3"):
                    if (d.data.name.toLowerCase().includes('group')) {
                        return d.data.name.toUpperCase().replace("GROUP", "");
                    }

            }
        });



    //текст внизу ячеек
    svg.selectAll("." + textArcElement)
        .data(pie(visualDonatData))
        .enter().append("text")
        .style("font-size", function (d, i) {
            if (d.data.id_group == 2) {
                return fontSizeInnerSlice / 1.5 + "px";
            } else {
                return fontSizeInnerSlice;
            }
        })


        .style("font-family", FONT_FAMILY_DONUT)
        .attr("class", textArcElement)
        .attr("pointer-events", "none")
        .append("textPath")
        .attr("href", function (d, i) {
            return "#" + ArcElement + i;
        })
        .attr("id", function (d, i) {
            return "text_bottom" + d.data.id_group;
        })
        .attr("startOffset", function (d, i) {
            return 50 + START_OFFSET_STADIUM + "%";

        })
        .style("fill", function (d, i) {
            switch (d.data.id_group) {
                case("2"):
                    return STADIUM_COLOR;
            }
        })
        .attr("text-anchor", function (d, i) {
            if (d.data.id_group == 0) {
                return "end";
            } else {
                return "start";
            }
        })

        //.attr("xlink:href",function(d) {return d.data.image;})
        .text(function (d, i) {
            if (d.data.id_group == 2) {
                return d.data.name.split(";")[0];
            } else {
                return "";
            }
        })
        .each(function (d, i) {
            let place_size =  d3.select("#" + ArcElement + i).node().getTotalLength();
            fit_text_in_rect(place_size, this);
        });
}