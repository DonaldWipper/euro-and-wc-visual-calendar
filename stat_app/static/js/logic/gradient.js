//Some random data
function linear_gradent(color_from, color_to, name) {
    var gradient = svg.append("svg:defs")
        .append("svg:linearGradient")
        .attr("id", name)
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "100%")
        .attr("y2", "100%")
        .attr("spreadMethod", "pad");

    // Define the gradient colors
    gradient.append("svg:stop")
        .attr("offset", "0%")
        .attr("stop-color", color_from)
        .attr("stop-opacity", 1);

    gradient.append("svg:stop")
        .attr("offset", "100%")
        .attr("stop-color", color_to)
        .attr("stop-opacity", 1);
}