function roundedRect(x, y, width, height, radius) {
    return "M" + x + "," + y
        + "h" + (width - radius)
        + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
        + "v" + (height - 2 * radius)
        + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
        + "h" + (radius - width)
        + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + -radius
        + "v" + (-height + 2 * radius)
        + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
        + "z";
}

function sortOnKeys(dict) {

    var sorted = [];
    for(var key in dict) {
        sorted[sorted.length] = key;
    }
    sorted.sort();

    var tempDict = {};
    for(var i = 0; i < sorted.length; i++) {
        tempDict[sorted[i]] = dict[sorted[i]];
    }

    return tempDict;
}



function draw_groups(games) {
    let groups = {};
    let stage_name;
    for (let i = 0; i < games.length; i++) {
        stage_name = games[i]["stage_name"]

        games[i]["away"] = games[i]["teamAway"]
        games[i]["home"] = games[i]["teamHome"]
        games[i]["result"] = games[i]["result"].replace(':', '-')
        if (stage_name in groups) {
            groups[stage_name].push(games[i])
        } else {
            groups[stage_name] = [games[i]]
        }
    }


    let groups_clear = {};
    for (let [key, value] of Object.entries(groups)) {
        if (key.toLowerCase().includes('group')) {
            groups_clear[key] = getDataFromMatches(value)
        }
    }
    // groups_clear.sort();
    let number_groups = Object.keys(groups_clear).length;
    let borders = getTableauBorders(innerRadius, TABLEAU_UP_Y)

    let space = innerRadius / 30;

    let canvasHeight = borders[1] * GROUPS_COEFF;
    let canvasWidth = borders[0]
    let tableHeight = canvasHeight
    let tableWigth =  canvasWidth/9;

    let y_start = -borders[1]/2;
    // svg.append("rect")
    //     .attr("width", canvasWidth)
    //     .attr("height", canvasHeight )
    //     .attr("class", "hour bordered")
    //     .attr("x", -canvasWidth/2)
    //     .attr("y", y_start)
    //     .attr("fill", "green")
    //     .attr("rx", 4)
    //     .attr("ry", 4)


    canvasWidth = number_groups * tableWigth + (number_groups - 1) * space

    let i = 0;

    groups_clear = sortOnKeys(groups_clear);
    for (let [key, value] of Object.entries(groups_clear)) {
        tabulate(key, value, i, y_start, tableWigth, tableHeight, canvasWidth, space)
        i += 1
    }
}

function clear_groups() {
    let class_elems = ['group_elems', 'hour_bordered']
    for (let name in class_elems) {
        d3.selectAll("." + class_elems[name]).remove();
        d3.selectAll("." + class_elems[name]).empty();
    }
}



function getIntersectionY(x) {
    let y = Math.sqrt(innerRadius * innerRadius - x * x)
    return [-y, y];
}


function tabulate(group_name, data, i, y_start, tableWigth, tableHeight, canvasWidth, space) {
    let x_start = -canvasWidth / 2 + i * (tableWigth + space)

    let shift = tableHeight / 5


    svg.append("rect")
        .attr("width", tableWigth)
        .attr("height", tableHeight)
        .attr("class", "group_elems")
        .attr("x", x_start)
        .attr("y", y_start)
        .attr("id", "group_rect")
        .attr("rx", 4)
        .attr("ry", 4)
        .attr("fill", COLOR_GROUP)
        .style("opacity", 0.5)

    svg.append("text")
        .text(function (d) {
            return group_name.toUpperCase();
        })
        .attr("class", "group_elems")
        .attr("x", x_start + tableWigth/2)
        .attr("y", y_start + shift / 2)
        .attr("id", "group_header")
        .style("font-family", FONT_FAMILY_DONUT)
        .style("font-size", shift * 0.7)
        .attr('text-anchor', 'middle')
        .attr("fill", BACK_RECT_COLOR_ON_CLICK)

    for (let i = 0; i < data.length; i++) {
        svg.append("text")
            .text(function (d) {
                return data[i]['NAME'];
            })
            .attr("class", "group_elems")
            .attr("x", x_start)
            .attr("y", y_start -  shift / 2  + (i + 2) * shift)
            .attr("id", "group_text1" + i)
            .style("font-family", FONT_FAMILY_DONUT)
            .style("font-size", shift * 0.6)
            .attr("fill", BACK_RECT_COLOR)
            .each(function (d) {
                let place_size = tableWigth;
                fit_text_in_rect(place_size, this);
            })
            .attr('text-anchor', 'start')
            .append("tspan")
            .attr("x", x_start + tableWigth)
            .attr("y", y_start -  shift / 2  + (i + 2) * shift)
            .attr("id", function (d) {
                return  "group_text2"  + i;
            })
            .style("font-family", FONT_FAMILY_DONUT)
            .style("font-weight", "bold")
            .style("fill", function (d) {
                return "white"
            })
            .attr('text-anchor', 'end')
            .text(function (d) {
                return data[i]['P'];
            })
    }


}

