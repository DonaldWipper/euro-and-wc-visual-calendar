function clear_playoff_data() {
    let elems = ['#scheme']
    for (let name in elems) {
        d3.select(elems[name]).remove();
        d3.select(elems[name]).empty();
    }
}



function inital_template() {
    let borders = getTableauBorders(innerRadius, TABLEAU_UP_Y)

    var iDiv = document.createElement('div');
    iDiv.id = 'scheme';
    iDiv.className = 'scheme';
    document.body.appendChild(iDiv);

    let playoffWidth = borders[0];
    let playoffHeight = borders[1] * PLAYOFF_COEFF;



    // let playoffHeight = 2 * Math.abs(getIntersectionY(PLAYOFF_WIDTH)[0])
    let coeffLayerSpacing = 0.5;
    let columnWidth = playoffWidth / (7 + 7 * coeffLayerSpacing);
    let layerSpacing = columnWidth * coeffLayerSpacing ;


    let coeffNodeSpacing = 0.5;
    let rowHeight = playoffHeight/ (8 + 7 * coeffNodeSpacing);
    let nodeSpacing = rowHeight * coeffNodeSpacing;


    let shift_y = Math.abs(borders[1] - playoffHeight)/2

    document.getElementById("scheme").style.width = playoffWidth + "px";
    document.getElementById("scheme").style.height = playoffHeight + "px";
    document.getElementById("scheme").style.left = width / 2 - playoffWidth / 2 + "px";
    document.getElementById("scheme").style.top = height / 2  - shift_y /2 + "px";

    document.getElementById("scheme").style.position = "relative";
    //document.getElementById("scheme").style.display = "none";
    document.getElementById("scheme").style.zIndex = "0"


    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
        $(go.Diagram, "scheme",
            {
                initialContentAlignment: go.Spot.MiddleTop,
                layout: $(DoubleTreeLayout,
                    {
                        //vertical: true,  // default directions are horizontal
                        // choose whether this subtree is growing towards the right or towards the left:
                        directionFunction: n => n.data && n.data.dir !== "left",
                        // initialContentAlignment: go.Spot.Right
                        // controlling the parameters of each TreeLayout:
                        bottomRightOptions: { nodeSpacing: nodeSpacing, layerSpacing: layerSpacing },
                        topLeftOptions: { nodeSpacing: nodeSpacing, layerSpacing: layerSpacing},
                    })
            });

    // myDiagram =
    //     $(go.Diagram, "scheme",  // create a Diagram for the DIV HTML element
    //         {
    //             initialContentAlignment: go.Spot.Right,  // center the content
    //             // "textEditingTool.starting": go.TextEditingTool.SingleClick,
    //             "textEditingTool.textValidation": isValidScore,
    //             layout: $(DoubleTreeLayout, {}),
    //             "undoManager.isEnabled": true
    //         });


    // validation function for editing text
    function isValidScore(textblock, oldstr, newstr) {
        if (newstr === "") return true;
        var num = parseInt(newstr, 10);
        return !isNaN(num) && num >= 0 && num < 1000;
    }

    myDiagram.nodeTemplate =
        $(go.Node, "Auto",
            {selectable: false},
            $(go.Shape, "Rectangle",
                {fill: HAT_RECT_COLOR_RIGHT, stroke: null},
                // Shape.fill is bound to Node.data.color
                new go.Binding("fill", "color")),

            $(go.Panel, "Table",
                $(go.RowColumnDefinition, {column: 0, separatorStroke: "black", height: columnWidth * 2 / 3}),
                $(go.RowColumnDefinition, {
                    column: 1,
                    // separatorStroke: "black",
                    height: columnWidth * 1 / 3,
                    background: SLICE_DEFAULT_COLOR


                }),
                $(go.RowColumnDefinition, {row: 0, separatorStroke: "black", height: rowHeight}),
                $(go.RowColumnDefinition, {row: 1, separatorStroke: "black", height: rowHeight}),
                $(go.TextBlock, "",
                    {
                        column: 0, row: 0,
                        wrap: go.TextBlock.None,
                        isMultiline: false, textAlign: 'left',
                        font:  (rowHeight * 0.75) + 'px ' + FONT_FAMILY_PLAYOFF, stroke: 'white',
                        alignment: go.Spot.Left
                    },
                    new go.Binding("text", "HomeShortName").makeTwoWay()),

                $(go.Picture,
                    {row: 0, width: rowHeight, height: rowHeight, alignment: go.Spot.Right},
                    new go.Binding("source", "imageHome")
                ),
                $(go.TextBlock, "",
                    {
                        column: 1, row: 0,
                        wrap: go.TextBlock.None,
                        isMultiline: false, editable: true, textAlign: 'center',
                        font: (rowHeight * 0.75) + 'px ' + FONT_FAMILY_PLAYOFF + ';', stroke: 'black'
                    },
                    new go.Binding("text", "goalsHomeTeam").makeTwoWay()),


                $(go.TextBlock, "",
                    {
                        column: 0, row: 1,
                        wrap: go.TextBlock.None,
                        isMultiline: false, textAlign: 'left',
                        font:  (rowHeight * 0.75) + 'px ' + FONT_FAMILY_PLAYOFF, stroke: 'black',
                        alignment: go.Spot.Left
                    },
                    new go.Binding("text", "AwayShortName").makeTwoWay()),

                $(go.Picture,
                    {row: 1, width: rowHeight, height: rowHeight, alignment: go.Spot.Right},
                    new  go.Binding("source", "imageAway")
                ),


                $(go.TextBlock, "",
                    {
                        column: 1, row: 1,
                        wrap: go.TextBlock.None,
                        isMultiline: false, editable: true, textAlign: 'center',
                        font: (rowHeight * 0.75) + 'px ' + FONT_FAMILY_PLAYOFF, stroke: 'black'
                    },
                    new go.Binding("text", "goalsAwayTeam").makeTwoWay())



            )
        );
// define the Link template
    myDiagram.linkTemplate =
        $(go.Link,
            {
                routing: go.Link.Orthogonal,
                selectable: false
            },
            $(go.Shape, {strokeWidth: 1, stroke: 'black'}));

// Generates the original graph from an array of team names


}


function createPairs(teams) {
    if (teams.length % 2 !== 0) teams.push('(empty)');
    var startingGroups = teams.length / 2;
    var currentLevel = Math.ceil(Math.log(startingGroups) / Math.log(2));
    var levelGroups = [];
    var currentLevel = Math.ceil(Math.log(startingGroups) / Math.log(2));
    for (var i = 0; i < startingGroups; i++) {
        levelGroups.push(currentLevel + '-' + i);
    }
    var totalGroups = [];
    makeLevel(levelGroups, currentLevel, totalGroups, teams);
    return totalGroups;
}


function makeLevel(levelGroups, currentLevel, totalGroups, teams) {
    currentLevel--;
    var len = levelGroups.length;
    var parentKeys = [];
    var parentNumber = 0;
    var p = '';
    for (var i = 0; i < len; i++) {
        if (parentNumber === 0) {
            p = currentLevel + '-' + parentKeys.length;
            parentKeys.push(p);
        }
        if (teams !== null) {
            var p1 = teams[i * 2];
            var p2 = teams[(i * 2) + 1];
            totalGroups.push({
                key: levelGroups[i], parent: p, teamHome: p1, teamAway: p2, parentNumber: parentNumber
            });
        } else {
            totalGroups.push({key: levelGroups[i], parent: p, parentNumber: parentNumber});
        }
        parentNumber++;
        if (parentNumber > 1) parentNumber = 0;
    }
    // after the first created level there are no team names
    if (currentLevel >= 0) makeLevel(parentKeys, currentLevel, totalGroups, null)
}

function makeModel(teams_) {
    teams_copy = []
    team_third  = []
    for (let i = 0; i < teams_.length; i++) {
        if (teams_[i]["key"] != "12-0") {
            teams_copy.push(teams_[i])
        } else {
            team_third.push(teams_[i])
        }
    }
    var model2 = new go.TreeModel(team_third);
    var model = new go.TreeModel(teams_copy);


    model.addChangedListener(function (e) {
        if (e.propertyName !== 'goalsHomeTeam' && e.propertyName !== 'goalsAwayTeam') return;

        var data = e.object;
        if (isNaN(data.goalsHomeTeam) || isNaN(data.goalsAwayTeam)) return;
        // TODO: What happens if goalsHomeTeam and goalsAwayTeam are the same number?
        // both goalsHomeTeam and goalsAwayTeam are numbers,
        // set the name of the higher-score'd team in the advancing (parent) node
        // if the data.parentNumber is 0, then we set teamHome on the parent
        // if the data.parentNumber is 1, then we set teamAway
        var parent = myDiagram.findNodeForKey(data.parent);
        if (parent === null) return;
        var teamName = parseInt(data.goalsHomeTeam) > parseInt(data.goalsAwayTeam) ? data.teamHome : data.teamAway;
        if (parseInt(data.goalsHomeTeam) === parseInt(data.goalsAwayTeam)) teamName = "";
        myDiagram.model.setDataProperty(parent.data, (data.parentNumber === 0 ? "teamHome" : "teamAway"), teamName);
    });

    myDiagram.model = model;
    // provide initial scores for at most three pairings
    /*
    for (var i = 0; i < teams.length); i++) {
      var d = model.nodeDataArray[i];
      if (d.teamHome && d.teamAway) {
        // TODO: doesn't prevent tie scores
        model.setDataProperty(d, "goalsHomeTeam", Math.floor(Math.random() * 100));
        model.setDataProperty(d, "goalsAwayTeam", Math.floor(Math.random() * 100));
      }
    }
    */
}

function show_playoff() {
    inital_template()
    makeModel(games_playoff);
}





