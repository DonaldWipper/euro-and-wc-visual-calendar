function create_result() {
    // var datapoints = [
    //     {'name': 'New York', 'population': 19	},
    //     {'name': 'Texas', 'population': 26 },
    //     {'name': 'California', 'population': 38 },
    //     {'name': 'Florida', 'population': 20 },
    //     {'name': 'Illinois', 'population': 12	}
    // ];
    //
    // var svg = d3.select("body")
    //     .append("svg")
    //     .attr("width", 400)
    //     .attr("height", 200);
    //
    // var data = [{x1: 20, x2: 60, y1: 30, y2: 50},
    //     {x1: 50, x2: 80, y1: 100, y2: 150},
    //     {x1: 200, x2: 400, y1: 10, y2: 100}];
    //
    // var rects = svg.selectAll("foo")
    //     .data(data)
    //     .enter()
    //     .append("rect")
    //     .attr("x", d=> d.x1)
    //     .attr("y", d=> d.y1)
    //     .attr("width", d=> d.x2 - d.x1)
    //     .attr("height", d=> d.y2 - d.y1)
    //     .attr("fill", "teal");

    var svg = d3.select('svg');


    svg
        .append('defs')
        .append('pattern')
        .attr('id', 'diagonalHatch')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 4)
        .attr('height', 4)
        .append('path')
        .attr('d', 'M-1,1 l2,-2 M0,4 l4,-4 M3,5 l2,-2')
        .attr('stroke', '#000000')
        .attr('stroke-width', 1);

    svg.append('defs')
       .append("pattern")
        .attr('id', 'hash')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', '25')
        .attr('height', '25')
        .append("g").style("fill", "none")
        .style("stroke", "black")
        .style("stroke-width", 1)
        .append("path").attr("d", "M0,0 l25,25");
    // g.append("path").attr("d", "M25,0 l-25,25");

    var rectangles = svg.selectAll('rect')
        .data(datapoints)
        .enter()
        .append('rect')
        .attr('x', 75)
        .attr('y', function(d, i) { return i * 30; })
        .attr('height', 20)
        .attr('fill', 'url(#hash)')
        .attr('width', function(d) { return d['population'] * 3 ; });

    var annotations = svg.selectAll('text')
        .data(datapoints)
        .enter()
        .append('text')
        .attr('x', 65)
        .attr('y', function(d, i) { return i * 30 + 15; })
        .text(function(d) { return d['name']; })
        .attr('font-size', 12)
        .attr('text-anchor', 'end');


    var canvas = d3.select("svg")
        .attr("height", 600)
        .attr("width", 600);

    canvas.append("polygon")
        .transition()
        .duration(1000)
        .style("background-color", "red")
        .attr("points", "-50,-50 -50,50 50,50 50,-50")
        .attr("transform", "matrix(5.39 0 0 0.24 275.03 22.62)")
        .style("fill", "#403D2A")
        // .style("stroke", "black")
        // .style("strokeWidth", 2);

    canvas.append("text")
        .attr("x", 0)
        .attr("y", 0)
        .attr("dy", ".35em")
        .text("adad");


    canvas.append("polygon")
        .transition()
        .duration(1000)
        .style("background-color", "red")
        .attr("points", "-50,-50 -50,50 50,50 50,-50")
        .attr("transform", "matrix(1.15 0 0 0.17 275.57 33.09)")
        .style("fill", "#17150F")
        // .style("stroke", "black")
        // .style("strokeWidth", 2);

    canvas.append("polygon")
        .transition()
        .duration(1000)
        .style("background-color", "red")
        .attr("points", "-50,-50 -50,50 50,50 50,-50")
        .attr("transform", "matrix(5.4 0 0 0.11 274.7 16.05)" )
        .style("fill", "#847C5B")
        // .style("stroke", "black")
        // .style("strokeWidth", 2);

    canvas.append("polygon")
        .transition()
        .duration(1000)
        .style("background-color", "red")
        .attr("points", "-50,-50 -50,50 50,50 50,-50")
        .attr("transform", "matrix(1.45 0 0 0.15 273.56 13.24)")
        .style("fill", "#ACA06B")

    canvas.append("polygon")
        .transition()
        .duration(1000)
        .style("background-color", "red")
        .attr("points", "-50,-50 -50,50 50,50 50,-50")
        .attr("transform", "matrix(0.64 0 0 0.14 231.65 13.07)" )
        .style("fill", "#EDE09A")


    canvas.append("polygon")
        .transition()
        .duration(1000)
        .style("background-color", "red")
        .attr("points", "-50,-50 -50,50 50,50 50,-50")
        .attr("transform", "matrix(0.64 0 0 0.14 313.49 13.74)")
        .style("fill", "#9D8F56")


}




// <g transform="matrix(5.39 0 0 0.24 275.03 22.62)"  >
//     <polygon style="stroke: rgb(0,0,0); stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: none; fill-rule: nonzero; opacity: 1;"  points="-50,-50 -50,50 50,50 50,-50 " />
// </g>
// <g transform="matrix(1.15 0 0 0.17 275.57 33.09)"  >
//     <polygon style="stroke: rgb(239,6,6); stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: round; stroke-miterlimit: 4; fill: none; fill-rule: nonzero; opacity: 1;"  points="-50,-50 -50,50 50,50 50,-50 " />
// </g>
// <g transform="matrix(5.4 0 0 0.11 274.7 16.05)"  >
//     <polygon style="stroke: rgb(42,203,58); stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(0,0,255); fill-opacity: 0; fill-rule: nonzero; opacity: 1;"  points="-50,-50 -50,50 50,50 50,-50 " />
// </g>
// <g transform="matrix(1.45 0 0 0.15 273.56 13.24)"  >
//     <polygon style="stroke: rgb(0,0,0); stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: none; fill-rule: nonzero; opacity: 1;"  points="-50,-50 -50,50 50,50 50,-50 " />
// </g>
// <g transform="matrix(0.64 0 0 0.14 231.65 13.07)"  >
//     <polygon style="stroke: rgb(239,5,5); stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: none; fill-rule: nonzero; opacity: 1;"  points="-50,-50 -50,50 50,50 50,-50 " />
// </g>
// <g transform="matrix(0.63 0 0 0.14 313.49 13.74)"  >
//     <polygon style="stroke: rgb(239,5,5); stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: none; fill-rule: nonzero; opacity: 1;"  points="-50,-50 -50,50 50,50 50,-50 " />
// </g>
// </svg>