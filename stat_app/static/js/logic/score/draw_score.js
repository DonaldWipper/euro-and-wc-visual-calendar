function roundedRect(x, y, width, height, radius) {
    return "M" + x + "," + y
        + "h" + (width - radius)
        + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + radius
        + "v" + (height - 2 * radius)
        + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + radius
        + "h" + (radius - width)
        + "a" + radius + "," + radius + " 0 0 1 " + -radius + "," + -radius
        + "v" + (-height + 2 * radius)
        + "a" + radius + "," + radius + " 0 0 1 " + radius + "," + -radius
        + "z";
}


function hatRoundedRectLeft(x, y, width, height, radius) {
    return "M" + x + "," + y
        + "h" + (width / 2 - radius)
        //+ "a" + radius + "," + radius + " 0 0, 1 " + radius + "," + radius
        + "v" + (-height + radius)
        // + "a" + radius + "," + radius + " 0 0, 0 " + (-radius) + "," + (-radius)
        + "h" + (x + 2 * radius)
        + "a" + radius + "," + radius + " 0 0, 0 " + (-radius) + "," + (radius)
        + "z";
}

function hatRoundedRectRight(x, y, width, height, radius) {
    return "M" + (x + width / 2 - radius) + "," + y
        + "h" + (x + width - radius)
        + "v" + (-height + 2 * radius)
        + "a" + radius + "," + radius + " 0 0, 0 " + (-radius) + "," + (-radius)
        + "h" + (2 * radius - width / 2)
        // + "a" + radius + "," + radius + " 0 0, 0 " + (-radius) + "," + (radius)
        + "z";
}


function blink(text_elem) {
    text_elem.transition()
        .duration(1000)
        .style("fill", "rgb(255,255,255)")
        .transition()
        .duration(1000)
        .style("fill", "rgb(0,0,0)")
        .transition()
        .duration(1000)
        .style("fill", "rgb(255,255,255)")
        .on("end", blink)
}


function getIntersectionX(y) {
    let x = Math.sqrt(innerRadius * innerRadius - y * y)
    return [-x, x];
}

function pathMainRect(x, y, width, height, radius = innerRadius) {
    //-X, HEIGHT_RECT, WIDTH_RECT, HEIGHT_RECT
    let x_intersect_up = getIntersectionX(y)
    let x_intersect_btm = getIntersectionX(y + height)


    x = "M" + x_intersect_up[0] + "," + y
        + " A" + radius + "," + radius + " 0 0,0 " + x_intersect_btm[0] + " " + (y + height)
        + " L" + x_intersect_btm[1] + "," + (y + height)
        + " A" + radius + "," + radius + " 0 0,0 " + x_intersect_up[1] + " " + y
        + " Z"
    let text_path = "M" + x_intersect_btm[0] + "," + (y + height) + " L" + x_intersect_btm[1] + " " + (y + height) + " Z"

    return [x, text_path]

}

function draw_goals(i, y_pos, centre_score_width, centre_score_height, goals_font_size, score_array) {
    for (let j = 0; j < score_array.length; j++) {
        svg.append('text')
            .attr('x', -centre_score_width / 2)
            .attr('id', "score" + i + "_" + j)
            .attr('y', function () {
                return y_pos + (j + 2) * centre_score_height;
            })
            .attr("fill", FONT_FAMILY_SCORE_COLOR)
            .attr("dy", -fontSizeInnerSlice / 4)
            .text(function () {
                return score_array[j];
            })
            .attr('font-size', goals_font_size)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'start')
            .each(function (d) {
                let place_size = centre_score_width;
                fit_text_in_rect(place_size, this);
            });

    }
}

//Считаем все параметры в зависимости от основных SCORE_HEIGTH и SPACE_SCORE_SIZE, INFO_RECT_UP и кол-ва данных
function calculate_params(number_elems) {


    let scoreHeight;      // высота самого большого квадратика
    let centreScoreShift;  //темное окошко счета
    let centreScoreHeight; //высота темного окошка
    let y_start;          //старт первого элемента
    let imageWidth;


    let scoreTableHeight = SCORE_HEIGTH * (number_elems + SPACE_SCORE_SIZE_COEFF * (number_elems - 1))

    if (scoreTableHeight > INFO_RECT_UP * 2) {

        scoreHeight = (INFO_RECT_UP * 2) / (number_elems + SPACE_SCORE_SIZE_COEFF * (number_elems - 1));
        y_start = INFO_RECT_UP;

    } else {
        scoreHeight = SCORE_HEIGTH;
        y_start = scoreTableHeight / 2;
    }


    return {
        scoreHeight: scoreHeight,
        spaceScoreHeight: SPACE_SCORE_SIZE_COEFF * scoreHeight,
        centreScoreShift: CENTRE_SCORE_SHIFT_COEFF * scoreHeight,
        centreScoreHeight: CENTRE_SCORE_HEIGHT_COEFF * scoreHeight,
        y_start: y_start,
        centreScoreWidth: CENTRE_SCORE_WIDTH_COEFF * SCORE_WIDTH,
        hatScoreHeight: HAT_SCORE_HEIGHT_COEFF * scoreHeight,
        hatScoreWidth: HAT_SCORE_WIDTH_COEFF * SCORE_WIDTH,
        imageWidth: IMAGE_COEFF * scoreHeight,
        imagePositionX: IMAGE_POSITION_X_COEFF * innerRadius,
        imagePositionY: IMAGE_POSITION_Y_COEFF * scoreHeight,
        teamFontSize: TEXT_TEAM_COEFF * scoreHeight,
        scoreFontSize: TEXT_SCORE_COEFF * CENTRE_SCORE_HEIGHT_COEFF * scoreHeight,
        upperRectHeight: UPPER_RECT_COEFF * scoreHeight,

    }

}

//Путь пересечения с аркой
//Группы

function clear_all_elementes_scores() {
    let class_elems = ['score_elems']
    for (let name in class_elems) {
        d3.selectAll("." + class_elems[name]).remove();
        d3.selectAll("." + class_elems[name]).empty();
    }

}


function draw_score(data, columns) {
    clear_all_elems()
    clear_all_elems()

    //Паттерн заливки с точкаxми
    var pattern = svg.append('defs')
        .append('pattern')
        .attr('id', 'dots')
        .attr('patternUnits', 'userSpaceOnUse')
        .attr('width', 5)
        .attr('height', 5);

    pattern.append('rect')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', 12)
        .attr('height', 12)
        // .style("opacity", 0.8)
        .attr('fill', BACK_RECT_COLOR);

    pattern.append('circle')
        .attr('cx', 1)
        .attr('cy', 1)
        .attr('r', 1)
        .attr('fill', 'black');


    data.sort(function (a, b) {
        var keyA = a.date,
            keyB = b.date;
        // Compare the 2 dates
        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        if (keyA = keyB) {
            if (a.date < b.date) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    });


    let main_rect_y;
    let rect2;
    let rect_centr;


    let res = calculate_params(data.length);

    let scoreHeight = res.scoreHeight;
    let centreScoreShift = res.centreScoreShift;
    let centreScoreHeight = res.centreScoreHeight;
    let y_start = res.y_start;
    let centreScoreWidth = res.centreScoreWidth;
    let imageWidth = res.imageWidth;
    let imagePositionX = res.imagePositionX;
    let imagePositionY = res.imagePositionY;
    let teamFontSize = res.teamFontSize;
    let spaceScoreHeight = res.spaceScoreHeight;
    let scoreFontSize = res.scoreFontSize;
    let upperRectHeight = res.upperRectHeight;
    let hatScoreWidth = res.hatScoreWidth;
    let hatScoreHeight = res.hatScoreHeight;

    let text_score = null;
    for (let i = 0; i < data.length; i++) {

        main_rect_y = -y_start + i * (scoreHeight + spaceScoreHeight)


        rect2 = svg.append("path")
            .attr("d", pathMainRect(-SCORE_WIDTH / 2, main_rect_y, SCORE_WIDTH, 0)[0])
            .attr('fill', 'url(#dots)')
            .attr("class", "score_elems")
            .attr('id', 'main_rect' + i);

        rect2.transition().ease(d3.easeLinear)
            .duration(ANIMATION_DURATION)
            .attr("d", pathMainRect(-SCORE_WIDTH / 2, main_rect_y, SCORE_WIDTH, scoreHeight)[0])

        upper_rec = svg.append("path")
            .attr("d", pathMainRect(-SCORE_WIDTH / 2, main_rect_y, SCORE_WIDTH, upperRectHeight)[0])
            .attr('fill', UPPER_RECT_COLOR)
            .attr("stroke-width", 2)
            .attr("stroke", "res")
            .attr("class", "score_elems")
            .attr('id', 'upper_rect' + i);

        svg.append('text')
            .attr("x", -imagePositionX)
            .attr("class", "score_elems")
            .attr('id', "game_number" + i)
            .attr('y', function () {
                return main_rect_y + upperRectHeight - 1;
            })
            .attr("fill", "")
            .text(function () {
                return "Game " + data[i].game_order;
            })
            .attr('font-size', upperRectHeight)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'start')


        svg.append('text')
            .attr("x", imagePositionX)
            .attr("class", "score_elems")
            .attr('id', "city" + i)
            .attr('y', function () {
                return main_rect_y + upperRectHeight - 1;
            })
            .attr("fill", "")
            .text(function () {
                return data[i].city;
            })
            .attr('font-size', upperRectHeight)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'end')


        rect_centr = svg.append("path")
            .attr("d", roundedRect(-centreScoreWidth / 2, main_rect_y + centreScoreShift, 0, centreScoreHeight, 6))
            .attr('fill', SCORE_RECT_COLOR)
            .attr("class", "score_elems")
            .attr('id', 'centr_rect' + i)


            .on("mouseover", function () {

                d3.select(this).style("fill", BACK_RECT_COLOR_ON_CLICK);

                let events_array;
                let start_;
                if (data[i].events_text !== '') {
                    d3.select(this).raise();
                    d3.select("#score" + i).raise();
                    events_array = data[i].events_text.split(",")
                    start_ = -y_start + i * (scoreHeight + spaceScoreHeight)
                    d3.select('#centr_rect' + i).transition().ease(d3.easeLinear)
                        .duration('50')
                        .attr("d", roundedRect(-centreScoreWidth / 2, start_ + centreScoreShift, centreScoreWidth, centreScoreHeight * (events_array.length + 1), 6));
                    draw_goals(i, start_ + centreScoreShift, centreScoreWidth, centreScoreHeight, scoreFontSize, events_array)
                }


            })
            .on("mouseout", function () {
                d3.select(this).style("fill", SCORE_RECT_COLOR);
                let start_;
                let events_array;
                if (data[i].events_text !== '') {
                    start_ = -y_start + i * (scoreHeight + spaceScoreHeight)
                    d3.select('#centr_rect' + i).transition().ease(d3.easeLinear)
                        .duration(ANIMATION_DURATION)
                        .attr("d", roundedRect(-centreScoreWidth / 2, start_ + centreScoreShift, centreScoreWidth, centreScoreHeight, 6))

                    events_array = data[i].events_text.split(",")
                    for (let j = 0; j < events_array.length; j++) {
                        d3.select("#score" + i + "_" + j).remove();
                        d3.select("#score" + i + "_" + j).empty();
                    }

                }
            });


        rect_centr.transition().ease(d3.easeLinear)
            .duration(ANIMATION_DURATION)
            .attr("d", roundedRect(-centreScoreWidth / 2, main_rect_y + centreScoreShift, centreScoreWidth, centreScoreHeight, 6))


        svg.append("image")
            .attr("width",
                imageWidth)
            .attr("height", imageWidth)
            .attr("x", -imagePositionX - imageWidth)
            .attr('y', function () {
                return main_rect_y + imagePositionY;
            })
            .attr("id", "image_team1" + i)
            .attr("class", "score_elems")
            .attr("xlink:href", function () {
                if (data[i].imageHome.includes('/.')) {
                    return svgDefaultImage;
                } else {
                    return data[i].imageHome;
                }
            });

        svg.append("image")
            .attr("width",
                imageWidth)
            .attr("height", imageWidth)
            .attr("x", imagePositionX)
            .attr('y', function () {
                return main_rect_y + imagePositionY;
            })
            .attr("class", "score_elems")
            .attr("id", "image_team2" + i)
            .attr("xlink:href", function () {
                if (data[i].imageAway.includes('/.')) {
                    return svgDefaultImage;
                } else {
                    return data[i].imageAway;
                }
            });

        svg.append('text')
            .attr("x", -imagePositionX)
            .attr("class", "score_elems")
            .attr('id', "team_name1" + i)
            .attr('y', function () {
                return main_rect_y + scoreHeight;
            })
            .attr("fill", FONT_FAMILY_SCORE_COLOR)
            .attr("dy", -teamFontSize / 4)
            .text(function () {
                return data[i].teamHome.toUpperCase();
            })
            .attr('font-size', teamFontSize)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'start')
            .each(function (d) {
                let place_size = imagePositionX - centreScoreWidth / 2;
                fit_text_in_rect(place_size, this);
            });


        svg.append('text')
            .attr('x', imagePositionX)
            .attr("class", "score_elems")
            .attr('id', "team_name2" + i)
            .attr('y', function () {
                return main_rect_y + scoreHeight;
            })
            .attr("fill", FONT_FAMILY_SCORE_COLOR)
            .attr("dy", -teamFontSize / 4)
            .text(function () {
                return data[i].teamAway.toUpperCase();
            })
            // .attr('font-size', teamFontSize)
            .attr('font-size', teamFontSize)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'end')
            .each(function (d) {
                let place_size = imagePositionX - centreScoreWidth / 2;
                fit_text_in_rect(place_size, this);
            });

        text_score = svg.append('text')
            .attr('x', 0)
            .attr("class", "score_elems")
            .attr('id', "score" + i)
            .attr('y', function () {
                return main_rect_y + centreScoreShift + centreScoreHeight;
            })
            .attr("fill", FONT_FAMILY_SCORE_COLOR)
            .attr("dy", -fontSizeInnerSlice / 4)
            .text(function () {
                return data[i].result.toUpperCase();
            })
            .attr('font-size', scoreFontSize)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'middle')
            .on("mouseover", function () {

                d3.select('#centr_rect' + i).style("fill", BACK_RECT_COLOR_ON_CLICK);
                let events_array;
                if (data[i].events_text != '') {

                    d3.select('#centr_rect' + i).raise();
                    d3.select("#score" + i).raise();
                    start_ = -y_start + i * (scoreHeight + spaceScoreHeight)
                    events_array = data[i].events_text.split(",")

                    d3.select('#centr_rect' + i).transition().ease(d3.easeLinear)
                        .duration('50')
                        .attr("d", roundedRect(-centreScoreWidth / 2, start_ + centreScoreShift, centreScoreWidth, centreScoreHeight * (events_array.length + 1), 6));
                    draw_goals(i, start_ + centreScoreShift, centreScoreWidth, centreScoreHeight, scoreFontSize, events_array);

                }
            })
            .on("mouseout", function () {
                d3.select('#centr_rect' + i).style("fill", SCORE_RECT_COLOR);
                let events_array;
                if (data[i].events_text != '') {
                    start_ = -y_start + i * (scoreHeight + spaceScoreHeight)
                    d3.select('#centr_rect' + i).transition().ease(d3.easeLinear)
                        .duration(ANIMATION_DURATION)
                        .attr("d", roundedRect(-centreScoreWidth / 2, start_ + centreScoreShift, centreScoreWidth, centreScoreHeight, 6))

                    events_array = data[i].events_text.split(",")

                    for (let j = 0; j < events_array.length; j++) {
                        d3.select("#score" + i + "_" + j).remove();
                        d3.select("#score" + i + "_" + j).empty();
                    }
                }

            });

        if (data[i].status == 'PLAYING') {
            blink(text_score)
        }

        hat_rect_1 = svg.append("path")
            .attr("d", hatRoundedRectLeft(-hatScoreWidth / 2, main_rect_y + upperRectHeight, hatScoreWidth, hatScoreHeight, 6))
            .attr('fill', HAT_RECT_COLOR_LEFT)
            .attr("class", "score_elems")
            .attr('id', 'hat_rect1' + i)
            .on("mouseover", function (d) {

            })

        hat_rect_2 = svg.append("path")
            .attr("d", hatRoundedRectRight(-hatScoreWidth / 2, main_rect_y + upperRectHeight, hatScoreWidth, hatScoreHeight, 6))
            .attr('fill', HAT_RECT_COLOR_RIGHT)
            .attr("class", "score_elems")
            .attr('id', 'hat_rect2' + i)
            .on("mouseover", function (d) {

            })

        svg.append('text')
            .attr("x", -hatScoreWidth / 4)
            .attr("class", "score_elems")
            .attr('id', "date" + i)
            .attr('y', function () {
                return main_rect_y + upperRectHeight + (upperRectHeight - hatScoreHeight * HAT_TEXT_SCORE_COEFF) / 2;
            })
            .attr("fill", "")
            .attr("dy", FONT_FAMILY_SCORE / 4)
            .text(function () {
                return convertUtcToLocal(data[i].date2 + " " + data[i].time);
            })
            .attr('font-size', hatScoreHeight * HAT_TEXT_SCORE_COEFF)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'middle')

        svg.append('text')
            .attr("x", hatScoreWidth / 4)
            .attr("class", "score_elems")
            .attr('id', "stage" + i)
            .attr('y', function () {
                return main_rect_y + upperRectHeight + (upperRectHeight - hatScoreHeight * HAT_TEXT_SCORE_COEFF) / 2;
            })
            .attr("fill", "black")
            .text(function () {
                return data[i].stage_name;
            })
            .attr('font-size', hatScoreHeight * HAT_TEXT_SCORE_COEFF)
            .attr('font-family', FONT_FAMILY_SCORE)
            .attr('text-anchor', 'middle')


    }
}

