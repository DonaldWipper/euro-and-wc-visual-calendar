function create_selected_list(tournaments, tournament_pos) {
    var element_id = "my-icon-select"
    var div_id = "myDiv2"

    var myDiv2 = document.getElementById(element_id);
    myDiv2.id = div_id;

    var selectList = document.createElement("select");
    selectList.id = "mySelect";
    selectList.style.width = width/4 + "px";
    selectList.style.fontSize = fontSizeInnerSlice * 1.5

    selectList.style.opacity = "0.6";
    selectList.style.fontFamily = FONT_FAMILY_SELECTED_LIST

    myDiv2.appendChild(selectList);


    for (var i = 0; i < tournaments.length; i++) {
        var option = document.createElement("option");
        option.style.fontFamily = FONT_FAMILY_SELECTED_LIST
        // option.style.opacity = "0.1";

        option.value = tournaments[i]['value'];
        option.text = tournaments[i]['text'];
        // img = "{{ url_for('static', filename= 'png/wc_icons/') }}" + option.value + ".png";
        option.style.opacity = 0.7;

        selectList.appendChild(option);
    }

    selectList.selectedIndex = tournament_pos

    $('#mySelect').change(function () {
        var optionSelected = document.getElementById('mySelect');
        getTournament(optionSelected.value)
    });
}