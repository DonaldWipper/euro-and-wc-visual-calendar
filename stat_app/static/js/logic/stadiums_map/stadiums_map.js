function clear_stadiums_map() {
    let class_elems = ['stadium_elems']
    for (let name in class_elems) {
        d3.selectAll("." + class_elems[name]).remove();
        d3.selectAll("." + class_elems[name]).empty();
    }
}



// var data_stad;
//
// d3.json(geoJSONMapPath, function (data) {
//     data_stad = data;
//
//     data_stad.features = data_stad.features.filter((d) => {
//         return d.properties.name == get_country(tournamentId);
//     });
//
//     // Здесь можно выполнять действия с отфильтрованными данными, например, отображение карты
//
//     console.log(data_stad.features); // Пример: вывод отфильтрованных данных в консоль
// });


function create_map() {

    let centre;
    let scale;

    const championship = championships.find((ch) => ch.id === tournamentId);
    centre = championship ? championship.center : [51.3, 25.1];
    scale =  championship ? championship.scale: innerRadius * 50;


    const projection = d3.geoMercator()
        // .center([51.3, 25.1])                // GPS of location to zoom on
        // .scale(innerRadius* 40)               // This is like the zoom
        // .translate([0, 0]);
        // .center([0, 0])                // GPS of location to zoom on
        // .scale(innerRadius* 40)               // This is like the zoom
        // .translate([0, 0]);
        .center(centre)                // GPS of location to zoom on
        .scale(scale)                       // This is like the zoom
        .translate([0, 0]);


    markers = []

    for (let i = 0; i < visualDonatData.length; i++) {

        if (visualDonatData[i]["id_group"] == 2) {
            if (visualDonatData[i]["long"] != "None") {
                markers.push(visualDonatData[i])
            }
        }

    }

    // d3.json(geoJSONMapPath, function (data) {
    //
    //     data.features = data.features.filter((d) => {
    //         return d.properties.name == get_country(tournamentId)
    //     })


    // Draw the map
    svg.append("g")
        .selectAll("path")
        .data(data_stad.features)
        .enter()
        .append("path")
        .attr("fill", function (d) {

            return SLICE_COLORS.stadium;
        })
        .attr("id", "map")
        .attr("class", "stadium_elems")
        .attr("d", d3.geoPath().projection(projection))
        .style('stroke', 'white')
        .style('stroke-width', 1.5)
        .style("opacity", 0.8)
        // tooltips
        .style("stroke", "white")
        .style('stroke-width', 0.3)
        .on('mouseover', function (d) {


            d3.select(this)
                .style("opacity", 1)
                .style("stroke", "white")
                .style("stroke-width", 3);
        })
        .on('mouseout', function (d) {


            d3.select(this)
                .style("opacity", 0.8)
                .style("stroke", "white")
                .style("stroke-width", 0.3);
        });

    svg.selectAll(".m")
        .data(markers)
        .enter()
        .append("image")
        .attr('width', innerRadius / 20)
        .attr("id", function (d, i) {
            return "marks" + i;
        })
        .attr("class", "stadium_elems")
        .attr('height', innerRadius / 20)
        .attr("xlink:href", markers[0].stadium_icon)
        .attr("transform", (d) => {
            let p = projection([d.long, d.lat]);
            return `translate(${p[0] - 10}, ${p[1] - 10})`
        })

    ;

    svg.append("g")
        .selectAll("text")
        .data(markers)
        .enter()
        .append("text")
        .text(function (d) {
            return d.name.split(";")[0];
        })

        .attr("opacity", 0.5)
        .attr('x', 0)
        .attr('y', 0)
        .attr("transform", (d) => {
            let p = projection([d.long, d.lat]);
            return `translate(${p[0] + 10}, ${p[1]})`
        })
        .attr("class", "calendar_elems")
        .style("font-family", FONT_FAMILY_DONUT)
        .style("font-size", innerRadius / 40)

    ;


}