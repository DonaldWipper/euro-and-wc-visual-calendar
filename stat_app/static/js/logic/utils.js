function clear_all_elems() {
    clear_all_elementes_scores(); //очки
    clear_stadiums_map();
    clear_playoff_data();
    clear_world_map();
    clear_groups();
    clear_calendar();
}

function getIntersectionY(x) {
    let y = Math.sqrt(innerRadius * innerRadius - x * x)
    return [-y, y];
}

function getIntersectionX(y) {
    let x = Math.sqrt(innerRadius * innerRadius - y * y)
    return [-x, x];
}


function calculateRectangleDimensions(radius, aspectRatio) {
    // Радиус окружности
    const R = radius;
    // Соотношение сторон
    const a = aspectRatio[0];
    const b = aspectRatio[1];

    // Вычисление масштабирующего коэффициента k
    const k = (2 * R) / Math.sqrt(a * a + b * b);

    // Вычисление ширины и высоты прямоугольника
    const width = k * a;
    const height = k * b;

    return {width: width, height: height};
}

function findPathExtremeCoordinates(pathData) {
    const longitudes = [];
    const latitudes = [];
    console.log( pathData.features)
    pathData.features.forEach(feature => {
        // console.log('extremum2');
        // console.log(feature);
        if (feature.geometry.type === "Polygon") {
            feature.geometry.coordinates.forEach(coords => {
                coords.forEach(coord => {
                    longitudes.push(coord[0]);
                    latitudes.push(coord[1]);
                });
            });
        } else if (feature.geometry.type === "MultiPolygon") {
            feature.geometry.coordinates.forEach(polygon => {
                polygon.forEach(coords => {
                    coords.forEach(coord => {
                        longitudes.push(coord[0]);
                        latitudes.push(coord[1]);
                    });
                });
            });
        }
    });


    const minLong = Math.min(...longitudes);
    const maxLong = Math.max(...longitudes);
    const minLat = Math.min(...latitudes);
    const maxLat = Math.max(...latitudes);


    return {minLong, maxLong, minLat, maxLat};
}

function getTableauBorders(radius, upper_y) {
    let boarders_x = getIntersectionX(upper_y)
    let boarders_y = getIntersectionX(boarders_x[0])
    return [2 * Math.abs(boarders_x[0]), 2 * Math.abs(boarders_y[0])]
}


function fit_text_in_rect(place_size, elem) {
    let cur_width = elem.getComputedTextLength();
    let font_size = d3.select(elem).style("font-size").replace('px', '');

    let res = cur_width > place_size;
    let i = 1000;

    while ((res === true) && (font_size > 1) && (i >= 0)) {
        font_size -= 0.1
        d3.select(elem).style("font-size", font_size + "px")
        cur_width = elem.getComputedTextLength();
        res = cur_width > place_size;
        i = i - 1;
    }
}

function getEllipseCoords(coorX, coorY, koefX, koefY, radius) {
    newX = radius * koefY * coorX * koefX / Math.sqrt(koefY * koefY * coorX * coorX + coorY * coorY * koefX * koefX);
    newY = newX * coorY / coorX;
    return [newX, newY]
}

function getPointDivided(point1, point2, k = 1.0) {
    x = (parseFloat(point1[0]) + k * parseFloat(point2[0])) / (k + 1)
    y = (parseFloat(point1[1]) + k * parseFloat(point2[1])) / (k + 1)
    return [x, y]
}

function getDistance(point1, point2) {
    return Math.sqrt(Math.pow(parseFloat(point1[0]) - parseFloat(point2[0]), 2) +
        Math.pow(parseFloat(point1[1]) - parseFloat(point2[1]), 2))
}

//угол наклона вектора относительно оси  OY
function getRotateAngle(point1, point2) {
    dist = getDistance(point1, point2)
    //угол(a, b) = arccos((a * b) / (|a| * |b|))
    vect1 = [point2[0] - point1[0], point2[1] - point1[1]]
    vect2 = [0, -1]
    mult = vect1[0] * vect2[0] + vect1[1] * vect2[1]
    d = mult / dist
    if (vect1[0] > 0) {
        return Math.acos(d) / Math.PI * 180
    } else {
        return -Math.acos(d) / Math.PI * 180
    }

}


function convertUtcToLocal(dateTimeStr) {
    // Парсим входную строку "DD.MM HH:MM"
    let [datePart, timePart] = dateTimeStr.split(' ');
    let [day, month] = datePart.split('.').map(Number);
    let [hours, minutes] = timePart.split(':').map(Number);

    // Получаем текущий год
    let now = new Date();
    let year = now.getUTCFullYear();

    // Создаем объект Date в UTC
    let utcDate = new Date(Date.UTC(year, month - 1, day, hours, minutes));

    // Преобразуем объект Date в локальное время
    let localDate = new Date(utcDate.toLocaleString('en-US', { timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone }));

    // Форматируем локальное время обратно в строку "DD.MM HH:MM"
    let localDay = localDate.getDate().toString().padStart(2, '0');
    let localMonth = (localDate.getMonth() + 1).toString().padStart(2, '0');
    let localHours = localDate.getHours().toString().padStart(2, '0');
    let localMinutes = localDate.getMinutes().toString().padStart(2, '0');

    return `${localDay}.${localMonth} ${localHours}:${localMinutes}`;
}

