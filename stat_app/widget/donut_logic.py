from collections import defaultdict
from typing import Dict, List
from widget.widget_data import JsonData
from operator import itemgetter


def get_date_time(date) -> (str, str):
    from datetime import datetime
    date_time_obj = datetime.strptime(date, "%Y-%m-%dT%H:%M:%SZ")
    date_str = date_time_obj.strftime("%Y-%m-%d")
    time_str = date_time_obj.strftime("%H:%M:%S")
    return date_str, time_str


def get_order(type_: str) -> int:
    dict_ = {
        "team": 0,
        "date": 1,
        "place": 2,
        "stage": 3
    }
    return dict_[type_]


def generate_indexes(cursor):
    indexes_dict = {}
    teams = sorted(list(set([c['home_team'] for c in cursor] + [c['away_team'] for c in cursor])), reverse=True)
    i = 0
    for t in teams:
        indexes_dict[t] = i
        i += 1

    dates = sorted(list(set([get_date_time(c['date'])[0] for c in cursor])))
    for d in dates:
        indexes_dict[d] = i
        i += 1

    cities = sorted(list(set([c['stadium'] for c in cursor])))

    for c in cities:
        indexes_dict[c] = i
        i += 1

    stages = list(set([s['stage'] for s in
                       sorted([{"id": c['stage_id'], "stage": c["stage"]} for c in cursor], key=itemgetter("id"),
                              reverse=True)]))

    for s in stages:
        indexes_dict[s] = i
        i += 1

    return indexes_dict


def generate_json_data(cursor) -> JsonData:
    indexes_dict = generate_indexes(cursor)
    teams = defaultdict(dict)
    stages = defaultdict(dict)
    dates = defaultdict(dict)
    places = defaultdict(dict)
    slices = {}
    matches = []
    matches_to_index = defaultdict(list)

    def generate_next_index(name: str) -> int:
        return indexes_dict[name]

    # def generate_next_index(type_: str) -> int:
    #     if type_ not in indexes_set:
    #         indexes_set.add(type_)
    #         return len(indexes_set) - 1
    #     else:
    #         return list(indexes_set).index(type_)

    def create_team_slice(team_name: str, image_path: str, teams: Dict, slices: Dict) -> int:
        if team_name not in teams:
            index_ = generate_next_index(team_name)
            slice = {
                "type": "team",
                "value": {"name": team_name},
                "index": index_,
                "dependencies": set(),
                "matches": [],
                "imagePath": image_path,
            }
            teams[team_name] = slice
            slices[index_] = slice
            return index_
        else:
            return teams[team_name]["index"]

    def create_place_slice(stadium: str, city: str, places: Dict, slices: Dict) -> int:
        if stadium not in places:
            index_ = generate_next_index(stadium)
            slice = {
                "type": "place",
                "value": {"city": city, "stadium": stadium},
                "index": index_,
                "dependencies": set(),
                "matches": [],
            }
            places[stadium] = slice
            slices[index_] = slice
            return index_
        else:
            return places[stadium]["index"]

    def create_date_slice(date_str: str, dates: Dict, slices: Dict) -> int:
        if date_str not in dates:
            index_ = generate_next_index(date_str)

            slice = {
                "type": "date",
                "value": {"date": date_str},
                "index": index_,
                "dependencies": set(),
                "matches": [],
            }
            dates[date_str] = slice
            slices[index_] = slice
            return index_
        else:
            return dates[date_str]["index"]

    def create_stage_slice(stage: str, stages: Dict, slices: Dict) -> int:
        if stage not in stages:
            index_ = generate_next_index(stage)
            slice = {
                "type": "stage",
                "value": {"name": stage},
                "index": index_,
                "dependencies": set(),
                "matches": [],
            }
            stages[stage] = slice
            slices[index_] = slice
            return index_
        else:
            return stages[stage]["index"]

    def generate_result(total_home_goals: int, total_away_goals: int,
                        penalty_shootout_home_goals: int, penalty_shootout_away_goals: int) -> str:
        if penalty_shootout_home_goals:
            return f"{total_home_goals}({penalty_shootout_home_goals}):{total_away_goals}({penalty_shootout_away_goals})"
        else:
            return f"{total_home_goals}:{total_away_goals}"

    def update_slice_dependencies(match_id: int, index: int, slices: Dict, matches_to_index: Dict):
        indexes = matches_to_index[match_id]
        for i in indexes:
            if i != index and slices[i]["type"] != slices[index]["type"]:
                slices[index]["dependencies"].add(i)

    for row in cursor:
        match_id = row["id"]
        competition = row["caption"]
        status = row["status"]
        year = row["year"]
        date = row["date"]
        local_date = row["local_date"]
        home_team = row["home_team"]
        away_team = row["away_team"]
        city = row["city"]
        stadium = row["stadium"]
        stage = row["stage"]
        total_home_goals = row["total_home_goals"]
        total_away_goals = row["total_away_goals"]
        penalty_shootout_home_goals = row["penalty_shootout_home_goals"]
        penalty_shootout_away_goals = row["penalty_shootout_away_goals"]
        text = row["text"]
        image_path_home = row["image_path_home"]
        image_path_away = row["image_path_away"]
        game_order = row["game_order"]

        competition_name = competition

        date_str, time_str = get_date_time(date)
        matches.append({
            "match_id": match_id,
            "status": status,
            "year": year,
            "date": date_str,
            "local_date": local_date,
            "time": time_str,
            "home_team": home_team,
            "away_team": away_team,
            "city": city,
            "stadium": stadium,
            "stage": stage,
            "total_home_goals": total_home_goals,
            "total_away_goals": total_away_goals,
            "penalty_shootout_home_goals": penalty_shootout_home_goals,
            "penalty_shootout_away_goals": penalty_shootout_away_goals,
            "text": text,
            "image_path_home": image_path_home,
            "image_path_away": image_path_away,
            "result": generate_result(total_home_goals, total_away_goals, penalty_shootout_home_goals,
                                      penalty_shootout_away_goals),
            "game_order": game_order
        })

        # Create the slice for home team
        create_team_slice(home_team, image_path_home, teams, slices)
        teams[home_team]["matches"].append(match_id)
        matches_to_index[match_id].append(teams[home_team]["index"])

        # Create the slice for away team
        create_team_slice(away_team, image_path_away, teams, slices)
        teams[away_team]["matches"].append(match_id)
        matches_to_index[match_id].append(teams[away_team]["index"])

        create_place_slice(stadium, city, places, slices)
        places[stadium]["matches"].append(match_id)
        matches_to_index[match_id].append(places[stadium]["index"])

        create_stage_slice(stage, stages, slices)
        stages[stage]["matches"].append(match_id)
        matches_to_index[match_id].append(stages[stage]["index"])

        create_date_slice(date_str, dates, slices)
        dates[date_str]["matches"].append(match_id)
        matches_to_index[match_id].append(dates[date_str]["index"])

        for index in matches_to_index[match_id]:
            update_slice_dependencies(match_id, index, slices, matches_to_index)

    # Sort the slices by type
    sorted_slices = sorted(slices.values(), key=lambda x: get_order(x["type"]))
    response = {}
    for slice in sorted_slices:
        slice["dependencies"] = list(slice["dependencies"])

    response['season'] = year
    response['competition'] = competition_name
    response['slices'] = sorted_slices
    response['matches'] = matches
    return response
