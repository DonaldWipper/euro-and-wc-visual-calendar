from typing import Optional
from pydantic import BaseModel, HttpUrl

from pydantic import BaseModel, Field
from typing import List, Dict


class Match(BaseModel):
    match_id: int
    status: str
    year: int
    date: str
    local_date: Optional[str] = None
    time: str
    home_team: str
    away_team: str
    city: str
    stadium: str
    stage: str
    total_home_goals: int
    total_away_goals: int
    penalty_shootout_home_goals: Optional[int] = None
    penalty_shootout_away_goals: Optional[int] = None
    text: Optional[str] = None
    image_path_home:  Optional[str] = None
    image_path_away:  Optional[str] = None
    result: str
    game_order: Optional[int] = None


class Slice(BaseModel):
    type: str
    value: Dict[str, str]
    index: int
    dependencies: List[int]
    matches: List[int]


class JsonData(BaseModel):
    season: int
    competition: str
    slices: List[Slice]
    matches: List[Match]
