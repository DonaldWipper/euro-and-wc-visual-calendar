GET_WIDGET_DATA_BY_ID = """
    SELECT         matches.id,
                   c.caption,
                   ms.status,
                   c.year,
                   date,
                   local_date,
                   t_home.name  AS home_team,
                   t_away.name  AS away_team,
                   places.city,
                   places.stadium,
                   stages.title AS stage,
                   matches.stage_id     AS stage_id, 
                   matches.total_home_goals,
                   matches.total_away_goals,
                   matches.penalty_shootout_home_goals,
                   matches.penalty_shootout_away_goals,
                   goals.text,
                   lower(concat(t_home.short_name2, '.svg')) as image_path_home,
                   lower(concat(t_away.short_name2, '.svg')) as image_path_away,
                   matches.game_order
            FROM matches
                     JOIN competitions c ON matches.competition_id = c.id
                     JOIN stages ON matches.stage_id = stages.id
                     JOIN teams t_home ON matches.home_team_id = t_home.id
                     JOIN teams t_away ON matches.away_team_id = t_away.id
                     JOIN match_status ms on matches.status_id = ms.id
                     JOIN places places ON matches.place_id = places.id
                     LEFT JOIN (SELECT match_id, GROUP_CONCAT(text ORDER BY id ASC SEPARATOR ',') AS text
                           FROM goals
                           GROUP BY match_id) AS goals ON matches.id = goals.match_id
            WHERE status_id = 2
              AND c.id = %s
"""

