from flask import Flask, render_template, make_response
from operator import itemgetter

from db_connection.sql import DBConnection

# from data_update.dashboard import Dashboard
from flask import request
import json

from datetime import datetime

cursor = None
competition_id = 1480

db = None
team_category = "NATIONAL"
tournamentPos = 0
events = []
stages = []
teams = []
places = []
rounds = []
games = []
dates = []
games_clear = []
games_update = []
games_update2 = []
games_playoff = []
tournaments = []
goals = []
space = None

teams_name_dic = {}

outGroups = [
    {"name": "NATIONAL TEAMS", "color": "#7E930A"},
    {"name": "COMPETITION SCHEDULE", "color": "#B20018"},
    {"name": "CITIES AND STADIUMS", "color": "#008399"},
    {"name": "GROUPS AND STAGES", "color": "#d7c119"},
]

dic_sliceId = {}
dic_name2sliceId = {}
dic_sliceId2name = {}

dic_slice_2_games = {}  # связь кусочка с играми
dic_games = {}  # все игры для вывода

# total_home_goals, total_away_goals, penalty_shootout_home_goals, penalty_shootout_away_goals


SQL_EVENTS = """SELECT id, text
FROM (SELECT match_id as id,
             m.competition_id,
                         GROUP_CONCAT(text ORDER BY goals.minute, goals.second, goals.id ASC SEPARATOR ',') AS text
      FROM goals
               JOIN {matches_table} m on goals.match_id = m.id
      GROUP BY match_id) as g"""

app = Flask(__name__)
app.config.from_object(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True


def sort_by_len(teams):
    mid = int(len(teams) / 2)

    teams_copy = teams.copy()
    teams_copy[mid] = teams[0]
    shift = 1
    i = 1

    while mid - shift >= 0 or mid + shift <= len(teams) - 1:
        if mid - shift >= 0:
            teams_copy[mid - shift] = teams[i]

        if mid + shift <= len(teams) - 1:
            teams_copy[mid + shift] = teams[i + 1]

        i += 2
        shift += 1
    return teams_copy


def xstr(s):
    if s is None:
        return ""
    return str(s)


def get_normal_date(date_str):
    return str(datetime.strptime(date_str.strip()[0:10], "%Y-%m-%d"))[0:10]


def get_connection_by_slice_id(slice_id):
    if slice_id not in dic_sliceId or slice_id not in dic_sliceId2name:
        return [-1]
    group_id = dic_sliceId[slice_id]
    if group_id == 0:
        return get_all_corell_by_team_id(dic_sliceId2name[slice_id])
    elif group_id == 1:
        return get_all_corell_by_date(dic_sliceId2name[slice_id])
    elif group_id == 2:
        return getAllCorellByPlace(dic_sliceId2name[slice_id])
    elif group_id == 3:
        return get_all_corell_by_stage(dic_sliceId2name[slice_id])


# календарь, стадионы, стадии
def get_all_corell_by_team_id(team_id):
    team_id = team_id
    places = [
        g["place_id"]
        for g in games
        if g["home_team_id"] == team_id or g["away_team_id"] == team_id
    ]
    dates = [
        get_normal_date(g["date"])
        for g in games
        if g["away_team_id"] == team_id or g["home_team_id"] == team_id
    ]
    _stages = [
        "s" + str(d["stage_id"])
        for d in games
        if d["away_team_id"] == team_id or d["home_team_id"] == team_id
    ]
    dic_slice_2_games[dic_name2sliceId[team_id]] += [
        g["id"]
        for g in games
        if g["away_team_id"] == team_id or g["home_team_id"] == team_id
    ]
    names = dates + _stages + places
    sliceIds = []
    for name in names:
        if name in dic_name2sliceId:
            sliceIds.append(dic_name2sliceId[name])
    return sliceIds


# по дате игры возвращаем
# группы, команды, места
def get_all_corell_by_date(date):
    places = [g["place_id"] for g in games if get_normal_date(g["date"]) == date]
    teams = [g["home_team_id"] for g in games if get_normal_date(g["date"]) == date] + [
        g["away_team_id"] for g in games if get_normal_date(g["date"]) == date
    ]
    _stages = [
        "s" + str(d["stage_id"]) for d in games if get_normal_date(d["date"]) == date
    ]
    dic_slice_2_games[dic_name2sliceId[date]] += [
        g["id"] for g in games if get_normal_date(g["date"]) == date
    ]
    names = teams + _stages + places
    sliceIds = []
    for name in names:
        if name in dic_name2sliceId:
            sliceIds.append(dic_name2sliceId[name])
    return sliceIds


# по месту возвращаем
# группы, команды, даты
def getAllCorellByPlace(place_id):
    dates = [get_normal_date(g["date"]) for g in games if g["place_id"] == place_id]
    teams = [g["home_team_id"] for g in games if g["place_id"] == place_id] + [
        g["away_team_id"] for g in games if g["place_id"] == place_id
    ]
    _stages = ["s" + str(d["stage_id"]) for d in games if d["place_id"] == place_id]
    dic_slice_2_games[dic_name2sliceId[place_id]] += [
        g["id"] for g in games if g["place_id"] == place_id
    ]
    names = dates + _stages + teams
    sliceIds = []
    for name in names:
        if name in dic_name2sliceId:
            sliceIds.append(dic_name2sliceId[name])
    return sliceIds


# по группе возвращаем
# места, команды, даты
def get_all_corell_by_stage(id_stage):
    teams = [
                g["home_team_id"] for g in games if "s" + str(g["stage_id"]) == id_stage
            ] + [g["away_team_id"] for g in games if "s" + str(g["stage_id"]) == id_stage]

    dates = [
        get_normal_date(g["date"])
        for g in games
        if "s" + str(g["stage_id"]) == id_stage
    ]
    places = [g["place_id"] for g in games if "s" + str(g["stage_id"]) == id_stage]

    dic_slice_2_games[dic_name2sliceId[id_stage]] += [
        g["id"] for g in games if "s" + str(g["stage_id"]) == id_stage
    ]
    names = dates + places + teams
    sliceIds = []
    for name in names:
        if name in dic_name2sliceId:
            sliceIds.append(dic_name2sliceId[name])
    return sliceIds


def get_playoff_data():
    global games_clear, games_playoff, stages
    result_playoff = []
    playoff_games = {}

    stages_names = [
        "Final",
        "3 AND 4 place",
        "Semi-finals",
        "Quarter-finals",
        "Round of 16",
    ]

    prev_stages = {21: 19, 19: 18, 18: 17}

    stages_dict = {s["title"]: s["id"] for s in stages if s["title"] in stages_names}
    stages_dict2 = {s["id"]: s["title"] for s in stages if s["title"] in stages_names}
    stages_ = list(
        reversed(list(set([s["id"] for s in stages if s["title"] in stages_names])))
    )

    for stage_id in stages_:
        # print(stage_id)
        pg = [g for g in games_clear if int(g["stage_id"]) == int(stage_id)]

        for g in pg:
            g["pair"] = frozenset([g["teamHome"], g["teamAway"]])
            g["key"] = None
            g["goals_home_team"] = g["goals_home_team_extra"]
            g["goals_away_team"] = g["goals_away_team_extra"]
            g["dir"] = "left"

        playoff_games[stage_id] = pg

    for stage_id in stages_:
        i = 0

        for g in playoff_games[stage_id]:
            g["key"] = f"{stage_id}-{i}"

            i += 1
            if stages_dict2[stage_id] == "3 AND 4 place":
                continue

            if stages_dict2[stage_id] == "Semi-finals":
                g["parent"] = f"{stages_dict['Final']}-{0}"
                if i == 1:
                    g["dir"] = "right"
                else:
                    g["dir"] = "left"

            if stage_id in prev_stages and prev_stages[stage_id] in playoff_games:
                for child in playoff_games[prev_stages[stage_id]]:

                    if (
                            g["teamHome"] in child["pair"] or g["teamAway"] in child["pair"]
                    ) and "parent" not in child:
                        child["parent"] = g["key"]
                        child["dir"] = g["dir"]

    for p in playoff_games:
        result_playoff = result_playoff + playoff_games[p]

    del playoff_games
    del games_playoff
    games_playoff = result_playoff

    merged_data = {}
    for d in games_playoff:
        if d['pair'] not in merged_data:
            merged_data[d['pair']] = d
        else:
            merged_data[d['pair']]['goals_home_team2'] = d['goals_away_team']
            merged_data[d['pair']]['goals_away_team2'] = d['goals_home_team']

    games_playoff = list(merged_data.values())
    del result_playoff


def init_data(_competition_id=competition_id):
    global stages, team_category, goals, games, teams, places, events, games_update, rounds, space, stages, db, dates, games_update2, games_playoff, games_clear, tournaments, tournamentPos

    # goals = DBConnection.get_dict_from_query(query=QOALS_STR + " WHERE g.tournamentId=%d" % int(_competition_id))
    tournaments = DBConnection.get_dict_from_table("competitions")
    tournaments = [t for t in tournaments if t["league"] in ("EC", "WC")]
    tournaments = sorted(tournaments, key=itemgetter("year"), reverse=True)

    team_category = [
        t["team_category"] for t in tournaments if t["id"] == int(_competition_id)
    ][0]
    matches_table = "matches"
    teams_table = "teams"
    if team_category == "CLUB":
        matches_table = "matches_clubs"
        teams_table = "teams_clubs"

    i = 0
    for t in tournaments:

        if 'champions league' not in t["caption"].lower():
            t["caption"] = t["caption"].replace(str(t["year"]), "")

        t["caption"] = str(t["year"]) + " " + t["caption"]

        t["position"] = i
        if int(t["id"]) == int(_competition_id):
            tournamentPos = i
        i += 1

    games = DBConnection.get_dict_from_table(
        matches_table, {"competition_id": _competition_id}
    )
    place_ids = set([g["place_id"] for g in games])
    stage_ids = set([g["stage_id"] for g in games])
    places = DBConnection.get_dict_from_table("places")
    places = [p for p in places if p["id"] in place_ids]
    places = sorted(places, key=itemgetter("city"))

    events = DBConnection.get_dict_from_query(
        query=SQL_EVENTS.format(matches_table=matches_table) + " WHERE g.competition_id=%d" % int(_competition_id)
    )

    stages = DBConnection.get_dict_from_table("stages")
    stages = [s for s in stages if s["id"] in stage_ids]
    stages = sorted(stages, key=itemgetter("id"), reverse=True)

    for s in stages:
        if s["title"] in [
            "Match for third place",
            "Play-off for third place",
            "Third-place play-off",
        ]:
            s["title"] = "3 AND 4 place"

    teams_ids = [g["home_team_id"] for g in games] + [g["away_team_id"] for g in games]

    teams = [
        team
        for team in DBConnection.get_dict_from_table(teams_table)
        if team["id"] in teams_ids
    ]

    # teams = sorted(teams, key=lambda x: -len(x['name']))
    # teams = sort_by_len(teams)
    teams = sorted(teams, key=itemgetter("name"), reverse=True)

    if team_category == "CLUB":
        teams = [
            {
                "short_name": d["country_short_name"],
                "short_name2": d["country_short_name"] + "-" + d["team_short_name"],
                **{k: v for k, v in d.items() if k not in ["team_short_name"]},
            }
            for d in teams
        ]

    dates = [str(r["date"]).strip()[0:10] for r in games]
    dates = sorted(list(set(dates)))
    # games_update = [str(g["home_team_id"]) + str(g["away_team_id"]) + getNormalDate(g["date"]) for g in games if g["status"] != "FINISHED" ]
    # games_update2 = [str(g["date"])  for g in games if g["status"] != "FINISHED" and int(g["home_team_id"]) == 757]
    res_update = None
    """
    try:
        get_update_data_by_league_id(competition_id)
    except:
        print("error of api request")
        res_update = None
    """
    # установим id, чтоб потом ссылаться
    i = 0
    del games_clear
    games_clear = []
    for g in games:
        g_new = get_game_dic(g)
        g["id"] = i
        g_new["id"] = g["id"]
        i += 1
        games_clear.append(g_new)

    get_playoff_data()



def get_result_string(g):
    if g["goals_home_team"] is None:
        return ""

    if g["total_home_goals"] is not None:
        home_goals = g["total_home_goals"]
        away_goals = g["total_away_goals"]
    else:
        home_goals = g["goals_home_team"]
        away_goals = g["goals_away_team"]

    if (
            int(g["penalty_shootout_home_goals"] or 0)
            + int(g["penalty_shootout_away_goals"] or 0)
    ) > 0:
        return f"{home_goals}({g['penalty_shootout_home_goals']}):{away_goals}({g['penalty_shootout_away_goals']})"
    else:
        return f"{home_goals}:{away_goals}"


def get_result_home(g):
    if g["goals_home_team"] is None:
        return ""

    if g["total_home_goals"] is not None:
        if g["penalty_shootout_home_goals"] is not None:
            return (
                    str(g["total_home_goals"])
                    + "("
                    + str(g["penalty_shootout_home_goals"])
                    + ")"
            )
        else:
            return str(g["total_home_goals"])
    else:
        return str(g["goals_home_team"])


def get_result_away(g):
    if g["goals_home_team"] is None:
        return ""
    if g["total_home_goals"] is not None:
        if g["penalty_shootout_home_goals"] is not None:
            return (
                    str(g["total_away_goals"])
                    + "("
                    + str(g["penalty_shootout_away_goals"])
                    + ")"
            )
        else:
            return str(g["total_away_goals"])
    else:
        return str(g["goals_away_team"])


def get_game_dic(g):
    game = {}
    try:
        p = [p for p in places if p["id"] == g["place_id"]][0]
        game["stadium"] = p["stadium"].split("|")[0].replace("Stadium", "")
        game["stadium"] = game["stadium"].replace("World Cup ", "")
        game["city"] = p["city"]
    except IndexError:
        game["stadium"] = ""
        game["city"] = ""

    try:
        game["events_text"] = [t for t in events if t["id"] == g["id"]][0]["text"]
    except IndexError:
        game["events_text"] = ""

    if g["home_team_id"] is not None:
        game["teamHome"] = [t for t in teams if t["id"] == g["home_team_id"]][0]["name"]
        game["teamHomeShortName2"] = [t for t in teams if t["id"] == g["home_team_id"]][
            0
        ]["short_name2"]
        game["teamHomeShortName"] = [t for t in teams if t["id"] == g["home_team_id"]][
            0
        ]["short_name"]
    else:
        game["teamHome"] = ""
        game["teamHomeShortName2"] = ""
        game["teamHomeShortName"] = ""

    if g["away_team_id"] is not None:
        game["teamAway"] = [t for t in teams if t["id"] == g["away_team_id"]][0]["name"]
        game["teamAwayShortName2"] = [t for t in teams if t["id"] == g["away_team_id"]][
            0
        ]["short_name2"]
        game["teamAwayShortName"] = [t for t in teams if t["id"] == g["away_team_id"]][
            0
        ]["short_name"]
    else:
        game["teamAway"] = ""
        game["teamAwayShortName2"] = ""
        game["teamAwayShortName"] = ""

    if g["goals_away_team"] is None:
        game["result"] = "upcoming" if g["status_id"] == 1 else ""
    else:
        game["result"] = get_result_string(g)

    game["date"] = g["date"][5:7] + "." + g["date"][8:10]
    game["date2"] = g["date"][8:10] + "." + g["date"][5:7]
    game["time"] = g["date"][11:16]

    game["stage_name"] = [t for t in stages if t["id"] == g["stage_id"]][0]["title"]
    game["goals_away_team"] = g["goals_away_team"]
    game["goals_home_team"] = g["goals_home_team"]
    game["goals_home_team_extra"] = (
        game["result"].split(":")[0] if ":" in game["result"] else ""
    )
    game["goals_away_team_extra"] = (
        game["result"].split(":")[1] if ":" in game["result"] else ""
    )
    game["stage_id"] = g["stage_id"]
    game["competition_id"] = g["competition_id"]
    game["game_order"] = g["game_order"]
    game["status"] = g["status_id"]
    return game


def render():
    global stages, team_category, teams, places, rounds, space, games, places, dic_slice_2_games, dates, tournaments, tournamentPos, goals
    sliceId = 0

    if len(places) > 16:
        shares = {
            "teams": 280,
            "calendar": 200,
            "places": 350,
            "stages": 150,
        }
    else:
        shares = {
            "teams": 350,
            "calendar": 600 // 3,
            "places": 650 // 3,
            "stages": 400 - 650 // 3,
        }

    space = (
                    1000
                    - (shares["teams"] + shares["calendar"] + shares["places"] + shares["stages"])
            ) // 4
    calendar = []
    # список команд
    for t in teams:
        t["value"] = shares["teams"] / len(teams)
        t["color"] = "#4daa4b"
        t["sliceId"] = sliceId
        t["id_group"] = 0
        teams_name_dic[t["id"]] = t["name"]
        dic_sliceId[sliceId] = 0
        dic_name2sliceId[t["id"]] = sliceId
        dic_sliceId2name[sliceId] = t["id"]
        sliceId += 1

    sliceId += 1  # для пространства
    # календарь игр
    for date in dates:
        c = {}
        strTime = get_normal_date(date.strip())
        Time = datetime.strptime(strTime.strip(), "%Y-%m-%d")
        c["time"] = strTime
        c["value"] = shares["calendar"] / len(dates)
        c["name"] = (
                Time.strftime("%d ")
                + Time.strftime("%A")[0:3]
                + ". "
                + Time.strftime("%B")[0:3]
                + "."
        )

        c["color"] = "#ddea4f"
        c["sliceId"] = sliceId
        c["id_group"] = 1
        dic_sliceId[sliceId] = 1
        dic_name2sliceId[strTime] = sliceId
        dic_sliceId2name[sliceId] = strTime
        sliceId += 1
        calendar.append(c)

    sliceId += 1  # для пространства
    # стадион + город
    for p in places:
        p["name"] = p["stadium"].split("|")[0] + ";" + p["city"]
        p["value"] = shares["places"] / len(places)
        p["color"] = "#4a69a9"
        p["sliceId"] = sliceId
        p["id_group"] = 2
        dic_sliceId[sliceId] = 2
        dic_name2sliceId[p["id"]] = sliceId
        dic_sliceId2name[sliceId] = p["id"]
        sliceId += 1

    sliceId += 1  # для пространства
    # раунды
    for s in stages:
        s["value"] = shares["stages"] / len(stages)
        s["color"] = "#a89449"
        s["sliceId"] = sliceId
        s["id_group"] = 3
        dic_sliceId[sliceId] = 3
        dic_name2sliceId["s" + str(s["id"])] = sliceId
        dic_sliceId2name[sliceId] = "s" + str(s["id"])
        sliceId += 1

    # какие игры показываем при клике
    for i in range(sliceId):
        dic_slice_2_games[i] = []

    click_events = []
    for curSlice in range(sliceId):
        click_events.append(
            {"key": curSlice, "value": list(set(get_connection_by_slice_id(curSlice)))}
        )

    slice_name = []
    for d in dic_slice_2_games:
        slice_name.append({"key": d, "value": list(set(dic_slice_2_games[d]))})

    dic_slice_2_games = {}

    return render_template(
        "stat_visual.html",
        teams=teams,
        rounds=calendar,
        places=places,
        stages=stages,
        space=space,
        outGroups=outGroups,
        click_events=click_events,
        games_clear=games_clear,
        slice_name=slice_name,
        games_playoff=games_playoff,
        tournaments=tournaments,
        tournamentPos=tournamentPos,
        tournamentCaption=tournaments[tournamentPos]["caption"],
        tournamentId=tournaments[tournamentPos]["id"],
        team_category=team_category
    )


def read_params(fn):
    d = {}
    try:
        with open(fn, "r", encoding="utf-8") as file:
            d = json.load(file)
    except FileNotFoundError:
        print("Error. Can't find file " + fn)
        d = {}
    return d


@app.route("/tour_stat", methods=["GET"])
def update():
    try:
        tournament_id = request.args.get("tournamentId")
        # x = Dashboard()
        # print(x)
        init_data(tournament_id)
        return render()
    finally:
        # DBConnection.DBConnection.close()
        pass


@app.route("/", methods=["GET"])
def main():
    try:
        if request.method == "POST":
            index = request.form["index"]
            # x = Dashboard()
            # print(x)
            init_data()
            render()
            resp = make_response(json.dumps({index: "succesfull"}))
            resp.status_code = 200
            resp.headers["Access-Control-Allow-Origin"] = "*"
            return resp
        else:
            init_data()

        return render()
    finally:
        pass
        # DBConnection.DBConnection.close()


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
